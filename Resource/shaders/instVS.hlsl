#include "OBJShaderHeader.hlsli"

VSOutput main(float4 pos : POSITION, float3 normal : NORMAL, float2 uv : TEXCOORD, uint inst : SV_InstanceID)
{
	// 法線にワールド行列によるスケーリング・回転を適用
	float4 wnormal = normalize(mul(matWorld[inst], float4(normal, 0)));
	float4 wpos = mul(matWorld[inst], pos);

	VSOutput output; // ピクセルシェーダーに渡す値
	output.svpos = mul(mul(viewproj, matWorld[inst]), pos);

	output.worldpos = mul(matWorld[inst], pos);
	output.normal = wnormal.xyz;
	output.uv = uv;
	output.inst = inst;


	return output;
}