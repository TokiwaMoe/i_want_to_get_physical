#include "bloom.hlsli"

Texture2D<float4> tex0 : register(t0);	//0番スロットに設定されたテクスチャ
Texture2D<float4> tex1 : register(t1);	//1番スロットに設定されたテクスチャ
SamplerState smp : register(s0);		//0番スロットに設定されたサンプラー


float Gaussian(float2 drawUV, float2 pickUV, float sigma)
{
	float d = distance(drawUV, pickUV);
	return exp(-(d * d) / (2 * sigma * sigma));
}

float4 main(VSOutput input) : SV_TARGET
{
	//高輝度部の抽出
	float4 col = tex0.Sample(smp, input.uv);
	float grayScale = col.r * 0.299 + col.g * 0.587 + col.b * 0.114;
	float extract = smoothstep(0.6, 0.9, grayScale);
	float4 Tex = col * extract;

	//ブラー
	float offsetU = 1.0f / 1280.0f;
	float offsetV = 1.0f / 720.0;

	col += tex0.Sample(smp, input.uv + float2(offsetU, 0.0f));
	col += tex0.Sample(smp, input.uv + float2(-offsetU, 0.0f));
	col += tex0.Sample(smp, input.uv + float2(0.0f, offsetV));
	col += tex0.Sample(smp, input.uv + float2(0.0f, -offsetV));
	col += tex0.Sample(smp, input.uv + float2(offsetU, offsetV));
	col += tex0.Sample(smp, input.uv + float2(offsetU, -offsetV));
	col += tex0.Sample(smp, input.uv + float2(-offsetU, offsetV));
	col += tex0.Sample(smp, input.uv + float2(-offsetU, -offsetV));

	col /= 9.0f;

	Tex += col;

	float4 col2 = tex0.Sample(smp, input.uv);
	float4 highLumi = Tex;

	return col2 * highLumi;

}