cbuffer cbuff0 : register(b0)
{
	float4 color;
	matrix mat;
	float time;
};

struct VSOutput {
	float4 svpos : SV_POSITION;	//システム用頂点座標
	float4 worldpos : POSITION;
	float2 uv : TEXCOORD;	//uv値
};
