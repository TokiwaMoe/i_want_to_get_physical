#include <d3d12.h>
#include <dxgi1_6.h>
#include <vector>
#include <string>
#include <cassert>
#include<sstream>
#include<iomanip>
#include <DirectXMath.h>
#include <d3dcompiler.h>
#define DIRECTINPUT_VERSION   0x0800
#include <dinput.h>
#include <math.h>
#include <DirectXTex.h>
#include<wrl.h>
#include<d3dx12.h>
#include<xaudio2.h>
#include<fstream>
#include"Input.h"
#include"WinApp.h"
#include"DirectXCommon.h"
#include"Object3d.h"
#include"Object3dModel.h"
#include"Sprite.h"
#include"DebugText.h"
#include"Collision.h"
#include"Audio.h"
#include"ParticleManager.h"
#include"DebugCamera.h"
#include"fbxsdk.h"
#include"FbxLoader.h"
#include"FbxObject3d.h"
#include"PostEffect.h"
#include"Light.h"
#include"Player.h"
#include"Enemy.h"
#include"Sword.h"
#include"GameScene.h"
#include"SceneManager.h"

#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dxguid.lib")
#pragma comment(lib, "xaudio2.lib")

using namespace DirectX;
using namespace Microsoft::WRL;


LRESULT CALLBACK WindowProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
	//メッセージで分岐
	switch (msg) {
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hwnd, msg, wparam, lparam);
}

//# Windouwsアプリでのエントリーポイント(main関数)
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	FbxManager* fbxManager = FbxManager::Create();

	WinApp* winApp = nullptr;

	//WindowsAPIの初期化
	winApp = new WinApp();
	winApp->Initialize();
	winApp->Finalize();
	winApp->ProcessMessage();

	//ポインタ置き場
	DirectXCommon* dxCommon = nullptr;
	dxCommon = new DirectXCommon();
	dxCommon->Initialize(winApp);
	//サウンド
	Audio* audio = nullptr;
	audio = new Audio();
	audio->Initialize();

	//ポインタ置き場
	Input* input = Input::GetInstance();
	input->Initialize(winApp);
	input->MouseInitialize(winApp);
	input->PadInitialize(winApp);
	//ゲームシーン
	
	SceneManager* sceneManager = nullptr;
	sceneManager = new SceneManager();
	sceneManager->Initialize(dxCommon, audio);

	while (true) {

		//Windowsのメッセージ処理
		if (winApp->ProcessMessage())
		{
			break;
		}

		//DirectX毎フレーム処理　ここから
		input->KeyUpdate();
		input->MouseUpdate();
		input->PadUpdate();
		//マウス座標
		POINT mousePos;
		GetCursorPos(&mousePos);

		//audio->PlayWave("Resources/Alarm01.wav");
	//オブジェクトの回転
		{
			/*XMFLOAT3 rot = objSphere->GetRotation();
			rot.y += 1.0f;
			objSphere->SetRotation(rot);
			objSphere2->SetRotation(rot);*/
		}

		//{
		//	//光線方向初期化
		//	static XMVECTOR lightDir = { 0,1.0f,5.0f,0 };

		//	if (input->PushKey(DIK_W)) { lightDir.m128_f32[1] += 1.0f; }
		//	else if (input->PushKey(DIK_S)) { lightDir.m128_f32[1] -= 1.0f; }
		//	if (input->PushKey(DIK_D)) { lightDir.m128_f32[0] += 1.0f; }
		//	else if (input->PushKey(DIK_A)) { lightDir.m128_f32[0] -= 1.0f; }

		//	light->SetLightDir(lightDir);
		//}
		//

		//for (int i = 0; i < 10; i++) {
		//	// X,Y,Z全て[-5.0f,+5.0f]でランダムに分布
		//	const float rnd_pos = 10.0f;
		//	XMFLOAT3 pos{};
		//	pos.x = (float)rand() / RAND_MAX * rnd_pos - rnd_pos / 2.0f;
		//	pos.y = (float)rand() / RAND_MAX * rnd_pos - rnd_pos / 2.0f;
		//	pos.z = (float)rand() / RAND_MAX * rnd_pos - rnd_pos / 2.0f;

		//	const float rnd_vel = 0.1f;
		//	XMFLOAT3 vel{};
		//	vel.x = (float)rand() / RAND_MAX * rnd_vel - rnd_vel / 2.0f;
		//	vel.y = (float)rand() / RAND_MAX * rnd_vel - rnd_vel / 2.0f;
		//	vel.z = (float)rand() / RAND_MAX * rnd_vel - rnd_vel / 2.0f;

		//	XMFLOAT3 acc{};
		//	const float rnd_acc = 0.001f;
		//	acc.y = -(float)rand() / RAND_MAX * rnd_acc;

		//	// 追加
		//	particleMan->Add(60, pos, vel, acc, 1.0f, 0.0f);
		//}

		
		sceneManager->Update();
		
#pragma region 3D描画
		
		sceneManager->Draw();
#pragma endregion
		

		//DirectX毎フレーム処理　ここまで

		// バッファをフリップ（裏表の入替え）
	}
	/*xAudio2.Reset();
	SoundUnload(&soundData1);*/
	//WindowAPIの終了処理
	winApp->Finalize();
	FbxLoader::GetInstance()->Finalize();
	//WindowAPI解放
	delete winApp;
	//DirectX解放
	delete dxCommon;
	//delete object1;
	//delete Object3dModel1;
	//delete postEffect;


	return 0;
}