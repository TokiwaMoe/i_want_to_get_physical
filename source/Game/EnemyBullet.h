#pragma once
#include"DirectXCommon.h"
#include"Camera.h"
#include"Object3d.h"
#include"Object3dModel.h"
#include"easing.h"
#include"Calculation.h"

class EnemyBullet
{
private:
	// Microsoft::WRL::を省略
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::を省略
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;
public:
	EnemyBullet();
	~EnemyBullet();
	void Initialize();//一回だけの初期化
	void Init(XMFLOAT3 position, XMFLOAT3 endPos, XMFLOAT3 speed, int num);//シーンチェンジ時にもする初期化
	void StraightBullet(int num);
	void CircleBullet(int num);
	void BoundBullet(Easing *easing, int num);
	void HomingInit(XMFLOAT3 position, XMFLOAT3 point1, XMFLOAT3 point2, XMFLOAT3 point3, XMFLOAT3 speed, int num);
	void Homing(Easing *easing, int num);
	void FlowerInit(XMFLOAT3 position, XMFLOAT3 speed, XMVECTOR distance, XMFLOAT3 proPos, float angle, int num);
	void FlowerBullet(XMFLOAT3 position);
	void Update();
	bool IsAlive() const { return isAlive; }
	void Draw();
	void OnCollision(int num);
	//setter
	void SetIsAlive(bool isAlive, int num) { this->isAlive[num] = isAlive; }
	void SetEnemyBulletPos(XMFLOAT3 position, int num) { this->enemyBulletPosition[num] = position; }
	//Getter
	XMFLOAT3 GetEnemyBulletPos(int num) { return enemyBulletPosition[num]; }
	bool GetIsAlive(int num) { return isAlive[num]; }
	float GetKTime(int num) { return boundTime[num]; }

private:
	//プレイヤーの動き系
	Object3dModel* enemyBulletModel = nullptr;
	Object3d* objEnemyBullet = nullptr;
	//共通計算クラス
	Calculation *calculation = nullptr;
	//弾の最大数
	static const int BULLET_MAX = 50;
	//位置サイズ角度
	XMFLOAT3 enemyBulletPosition[BULLET_MAX];
	XMFLOAT3 endPos[BULLET_MAX];
	XMFLOAT3 bulletSpeed = { 0,0,0 };
	
	//angle
	float angle = 0;
	//寿命<frm>
	static const int32_t KLifeTime = 60 * 5;
	//デスタイマー
	int32_t deathTimer_[BULLET_MAX];
	//デスフラグ
	bool isAlive[BULLET_MAX];
	//スピード
	XMFLOAT3 velocity;
	//flower座標
	XMFLOAT3 flowerPos;
	XMFLOAT3 petalPos;
	
	//速度
	XMFLOAT3 flowerVelocity;
	//デスフラグ
	bool isFlowerDeath = false;
	//バウンドタイム
	float boundTime[BULLET_MAX];

	//ホーミング
	XMFLOAT3 a[BULLET_MAX];
	XMFLOAT3 b[BULLET_MAX];
	XMFLOAT3 c[BULLET_MAX];
	XMFLOAT3 d[BULLET_MAX];
	XMFLOAT3 e[BULLET_MAX];

	struct Point
	{
		XMFLOAT3 p0 = { 0,0,0 };
		XMFLOAT3 p1 = { 0,0,0 };
		XMFLOAT3 p2 = { 0,0,0 };
		XMFLOAT3 p3 = { 0,0,0 };
	};

	Point point[BULLET_MAX];
	const float homingSpeed = 0.05f;
	float homingTime[BULLET_MAX];

	//球の弾幕
	//発射タイマー
	const float maxShotTime = 1.0f;
	float shotTime = 0;
	XMVECTOR pos = { 0,0,0.5f,0 };
	static const int BALL_MAX = 15;
	float ball_max;
	const float PI = 3.141592f;
	XMVECTOR distance[BULLET_MAX] = {0,0,0,0};
	float ballAngle[BULLET_MAX];
	XMFLOAT3 proPos[BULLET_MAX];
	//アングル
	float proBallAngle = 0;
};
