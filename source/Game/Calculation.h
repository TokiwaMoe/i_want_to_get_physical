#pragma once
#include <DirectXMath.h>
#include <string>
#include"FbxObject3d.h"

class Calculation
{
private:
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;
public:
	//XMFLOAT3にベクトルを足す
	static XMFLOAT3 AddVec(const XMFLOAT3& pos, const XMVECTOR& vec);
	//ワールド行列から座標を取得
	XMFLOAT3 GetBonePosition(std::string boneName, FbxObject3d *fbxObj);
	//前方ベクトルを求める
	XMVECTOR ForwardVector(float speed, XMFLOAT3 enemyPos, XMFLOAT3 playerPos);
	//右方向ベクトルを求める
	XMVECTOR RightVector(float speed, XMFLOAT3 enemyPos, XMFLOAT3 playerPos);
	//Y回転後の速度を求める
	XMFLOAT3 RotationYVelocity(XMVECTOR distance, float angle);
	//X回転後の速度を求める
	XMFLOAT3 RotationXVelocity(XMVECTOR distance, float angle);
	//z回転後の速度を求める
	XMFLOAT3 RotationZVelocity(XMVECTOR distance, float angle);
	//2点間の距離
	float Difference(XMFLOAT3 a, XMFLOAT3 b);
	//逆行列求める
	XMMATRIX InverseRot(XMFLOAT3 pos1, XMFLOAT3 pos2);
	//乱数
	uint64_t Get_rand_range(uint64_t min_val, uint64_t max_val);
};
