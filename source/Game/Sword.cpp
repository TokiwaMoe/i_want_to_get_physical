#include "Sword.h"
#include "Input.h"
#include <DirectXMath.h>
#include <d3dcompiler.h>
#include <DirectXTex.h>
#include<fstream>
#include<sstream>
#include<string>
#include<vector>

void Sword::Initialize(Enemy* enemy, DirectXCommon* dxCommon, Camera* camera)
{
	//fbx読み込み
	swordFbxModel = FbxLoader::GetInstance()->LoadMadelFromFile("sword");
	fbxsword = new FbxObject3d;
	fbxsword->Initialize();
	fbxsword->SetModel(swordFbxModel.get());
	fbxsword->SetScale({ 0.001f,0.001f,0.001f });
	fbxsword->SetPosition({ 0,0,0 });

	effects = new Effects();
	effects->Initialize(dxCommon->GetDev(), dxCommon->GetCmdQueue(), camera);
	effects->Load(L"effectsTest/10/fire_1.efk");
}

void Sword::Init()
{
}

void Sword::Update(Player* player, Enemy* enemy, DirectXCommon* dxCommon, Camera* camera)
{
	Move(player);
	fbxsword->Update();
	SwordEnemyCollision(enemy, dxCommon, camera, player);
}

void Sword::Move(Player* player)
{
	//回転行列セット
	if (player->isWalk || player->GetState() == Player::ATTACK)
	{
		fbxsword->SetIsRot(true);
		fbxsword->SetMatRot(player->GetInverseRot());
	}
	//プレイヤーの右手のボーン座標
	position = calculation->GetBonePosition("Bip01_R_Hand", player->fbxPlayer);

	fbxsword->SetPosition(position);
}

void Sword::SwordEnemyCollision(Enemy* enemy, DirectXCommon* dxCommon, Camera* camera, Player* player)
{
	//敵の当たり判定の座標
	enemySphere[0].center = {
		enemy->GetCollisionMAt().r[3].m128_f32[0],
		enemy->GetCollisionMAt().r[3].m128_f32[1] - 0.3f,
		enemy->GetCollisionMAt().r[3].m128_f32[2],
	};

	//剣の当たり判定の球の位置
	for (int i = 0; i < sword_Max; i++)
	{
		XMVECTOR v0_Sword = { 0, 0.1f, swordRadius * 2 * i };
		//angleラジアンだけy軸まわりに回転。半径は-100
		XMMATRIX rotM_Sword = DirectX::XMMatrixIdentity();
		rotM_Sword *= DirectX::XMMatrixRotationX(DirectX::XMConvertToRadians(rotation.x));
		rotM_Sword *= DirectX::XMMatrixRotationY(DirectX::XMConvertToRadians(rotation.y));
		rotM_Sword *= DirectX::XMMatrixRotationZ(DirectX::XMConvertToRadians(rotation.z));
		XMVECTOR v_sowrd = XMVector3TransformNormal(v0_Sword, rotM_Sword);
		XMVECTOR swordVec = { position.x, position.y, position.z };
		XMVECTOR swordPos = { swordVec.m128_f32[0] + v_sowrd.m128_f32[0], swordVec.m128_f32[1] + v_sowrd.m128_f32[1], swordVec.m128_f32[2] + v_sowrd.m128_f32[2] };

		swordSphere[i].center = swordPos;
		swordSphere[i].radius = swordRadius;
	}

	//判定
	for (int i = 0; i < sword_Max; i++)
	{
		isHit_enemy1[i] = Collision::CheckSphere2(swordSphere[i], enemySphere[0]);

		if (player->GetState() == Player::ATTACK || player->GetState() == Player::ATTACK_2 || player->GetState() == Player::ATTACK_3)
		{
			if (isHit_enemy1[i] && Decrease == false)
			{
				enemy->SetHP(enemy->GetHP() - 1);
				Decrease = true;
				enemy->SetIsColl(true);
				isEffect = true;

				if (enemy->GetHP() == 5)
				{
					enemy->SetIsColl(false);
				}
			}
		}
		else
		{
			Decrease = false;
		}

	}

	if (isEffect)
	{
		effectTime += 0.1f;
		effects->Play();
		effects->SetPosition({ enemy->GetCollisionMAt().r[3].m128_f32[0],enemy->GetCollisionMAt().r[3].m128_f32[1],enemy->GetCollisionMAt().r[3].m128_f32[2] });
		effects->SetScale({ 0.2f,0.2f,0.2f });
		effects->SetRotation({ 0,180,0 });
		effects->SetSpeed(5);
		effects->Update(dxCommon->GetCmdList(), camera);
		if (effectTime > effectMaxTime)
		{
			effects->Stop();
			isEffect = false;
			effectTime = 0;
		}
	}
}

void Sword::Draw(DirectXCommon* dxCommon)
{
	fbxsword->Draw(dxCommon->GetCmdList());
	effects->Draw(dxCommon->GetCmdList());
}

void Sword::SetPosition(XMFLOAT3 pos)
{
	this->position = pos;
}
