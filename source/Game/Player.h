#pragma once
#include <DirectXMath.h>
#include"DirectXCommon.h"
#include"Camera.h"
#include"Object3d.h"
#include"Object3dModel.h"
#include"Easing.h"
#include"fbxsdk.h"
#include"FbxLoader.h"
#include"FbxObject3d.h"
#include"DirectXCommon.h"
#include"Effects.h"
#include"FbxModel.h"
#include"Sprite.h"
#include"Calculation.h"

class Player {
private:
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;

public:
	enum Direction
	{
		Previous,
		Back,
		Right,
		Left
	};

	enum STATE
	{
		WAIT,
		MOVE,
		ATTACK,
		ATTACK_2,
		ATTACK_3,
		DODGE,
		DAMAGE,
		DEATH,
	};
public:
	void Initialize(DirectXCommon *dxCommon, Camera *camera);

	void Init();

	void Update(DirectXCommon* dxCommon, Camera* camera, XMFLOAT3 enemyPos);
	//リソースの更新
	void ResourceUpdate();
	//移動時の座標などをセット
	void SetMove(XMVECTOR moveVec, XMVECTOR rotVec);
	//移動時のキー入力の処理
	void Move(Camera* camera, XMFLOAT3 enemyPos);
	//回避の処理
	void Dodge(XMFLOAT3 enemyPos);
	//ノックバックの処理
	void knockBack(DirectXCommon* dxCommon, Camera* camera, XMFLOAT3 enemyPos);
	//攻撃の処理
	void Attack(XMFLOAT3 enemyPos);
	//コンボの処理
	void Combo(int min, int max, STATE type, XMFLOAT3 enemyPos);
	//死亡時の処理
	void Death();
	//3dの描画
	void Draw(DirectXCommon* dxCommon);
	//2dの描画
	void SpriteDraw();
	//HPバーのサイズ変更
	void SetHpSprite();
	//行動中のfbxの処理
	void SetMoveFbxTime(FbxTime frame, FbxTime maxTime);
	//アニメーション後の処理
	void SetEndAnimeInit(FbxTime *time = nullptr);
	//回転行列をモデルに合成
	void SetInverseRot(XMFLOAT3 enemyPos);

	//プレイヤーステート取得
	int GetState() { return state_; }

	//getter
	//座標
	const XMFLOAT3& GetPosition() { return position; }
	//回転
	const XMFLOAT3& GetRotation() { return rotation; }
	//HP
	int GetHP() { return HP; }
	//fbxのワールド座標
	XMMATRIX GetMatWorld() { return matWorld; }
	//ノックバックフラグ
	bool GetIsKnock() { return isKnock; }
	//死フラグ
	bool GetIsDeath() { return isDeath; }
	//逆行列
	XMMATRIX GetInverseRot() { return inverseRot; }
	//setter
	//HP
	int SetHP(int hp) { return HP = hp; }
	//ノックバックフラグ
	bool SetIsKnock(bool knock) { return isKnock = knock; }
	//座標
	XMFLOAT3 SetPosition(XMFLOAT3 position) { return this->position = position; }

private:
	static const int maxHP = 30;

public:
	//3d
	std::unique_ptr<FbxModel> player_FbxModel = nullptr;
	FbxObject3d* fbxPlayer = nullptr;

	//2d
	Sprite* HPBar;

	//その他
	Easing* easing = nullptr;
	Effects* effects = nullptr;
	Calculation* calculation;

	// ローカルスケール
	XMFLOAT3 scale = { 1,1,1 };
	// X,Y,Z軸回りのローカル回転角
	XMFLOAT3 rotation = { 0,0,0 };
	// プレイヤー座標
	XMFLOAT3 position = { 0,0,0 };
	XMFLOAT3 oldPos = { 0,0,0 };
	XMFLOAT3 endPos = { 0,0,0 };
	XMFLOAT3 storagePos = { 0,0,0 };
	
	//ノックバック
	float knockTime = 0;
	XMFLOAT3 knock_EndPos = { 0,0,0 };
	XMFLOAT3 knock_OldPos = { 0,0,0 };
	bool isKnock;
	int HP;
	//fbxTime
	FbxTime AnimationTime = 0;
	//move変数
	bool isWalk = false;
	///回転行列
	XMFLOAT3 rotPos = {};
	//ボーン取得変数
	XMMATRIX matWorld;
	//ゲームオーバー変数
	bool isDeath = false;
	//アタック変数
	bool isEffect = false;
	float effectTime = 0;
	const float effectMaxTime = 1;
	bool isCombo = false;
	int key_l = 0;
	const int KEY_MAX = 20;
	//逆行列
	XMMATRIX inverseRot;
	//行動管理
	STATE state_;
	//回避時の向いてる方向
	Direction direction;
	//回避
	float dodgetime = 0;
};