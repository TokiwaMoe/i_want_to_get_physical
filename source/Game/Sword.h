#pragma once
#include <DirectXMath.h>
#include"DirectXCommon.h"
#include"Camera.h"
#include"Object3d.h"
#include"Object3dModel.h"
#include"Easing.h"
#include"Player.h"
#include"Enemy.h"
#include"Collision.h"
#include"CollisionPrimitive.h"
#include"Effects.h"
#include"fbxsdk.h"
#include"FbxLoader.h"
#include"FbxObject3d.h"
#include"Calculation.h"

class Sword {
private:
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;
public:
	/*Sword();
	~Sword();*/

	void Initialize(Enemy *enemy, DirectXCommon* dxCommon, Camera* camera);
	
	void Init();

	void Update(Player* player, Enemy *enemy, DirectXCommon* dxCommon, Camera* camera);

	void Move(Player *player);

	void SwordEnemyCollision(Enemy* enemy, DirectXCommon* dxCommon, Camera* camera, Player *player);

	void Draw(DirectXCommon* dxCommon);

	void SetPosition(XMFLOAT3 pos);
	float GetAngle() { return Angle; }
	XMFLOAT3 GetRotation() { return rotation; }
	bool GetDecrease() { return Decrease; }
	XMFLOAT3 GetPosition() { return position; }

private:
	static const int sword_Max = 10;

public:
	std::unique_ptr<FbxModel> swordFbxModel = nullptr;
	FbxObject3d* fbxsword = nullptr;

	Effects* effects = nullptr;
	Calculation* calculation;
	
	// ローカルスケール
	XMFLOAT3 scale = { 1,1,1 };
	// X,Y,Z軸回りのローカル回転角
	XMFLOAT3 rotation = { 0,0,0 };
	// プレイヤー座標
	XMFLOAT3 position = { 0,0,0 };

	OBB swordOBB;
	OBB enemyOBB;
	bool Decrease = false;

	Sphere swordSphere[sword_Max];
	Sphere enemySphere[3];
	const float swordRadius = 0.08f;
	const float enemyRadius = 1.0f;
	bool isHit_enemy1[sword_Max];
	bool isHit_enemy2[sword_Max];
	bool isHit_enemy3[sword_Max];
	float Angle;
	bool isRote = false;
	XMFLOAT3 pos[3];
	XMFLOAT3 sword3 = { 0,0,0 };

	float effectTime = 0;
	const float effectMaxTime = 1;
	bool isEffect = false;

private:
	bool Attack = false;
};