#include "Enemy.h"
#include "Input.h"

using namespace DirectX;

Enemy::Enemy(){}
Enemy::~Enemy(){}

void Enemy::Initialize(DirectXCommon* dxCommon, Camera* camera)
{
	scale = { 0.015f,0.015f,0.015f };
	//fbx読み込み
	enemyModel = FbxLoader::GetInstance()->LoadMadelFromFile("enemy");
	fbxEnemy = new FbxObject3d;
	fbxEnemy->Initialize();
	fbxEnemy->SetModel(enemyModel.get());
	fbxEnemy->LoadAnumation();
	fbxEnemy->SetScale(scale);
	fbxEnemy->SetRotation({ 0,180,0 });
	//イージング
	easing = new Easing();
	easing->Initialize();
	//エフェクト
	effects = new Effects();
	effects->Initialize(dxCommon->GetDev(), dxCommon->GetCmdQueue(), camera);
	//hpスプライト
	Sprite::LoadTexture(6, L"Resource/enemyHP.png");
	HPBar = Sprite::Create(6, { 372.0f,660.0f });

	//ホーミング
	homingBullet = new EnemyBullet();
	homingBullet->Initialize();
	//バウンド
	boundBullet = new EnemyBullet();
	boundBullet->Initialize();
	//サークル
	for (int i = 0; i < CIRCLE_MAX; i++)
	{
		CircleBullet[i] = new EnemyBullet();
		CircleBullet[i]->Initialize();
	}
	//球
	for (int i = 0; i < COUNTER_MAX; i++)
	{
		flowerBullet[i] = new EnemyBullet();
		flowerBullet[i]->Initialize();
	}
	//直線
	for (int i = 0; i < STRAIGHT_MAX; i++)
	{
		straightBullet[i] = new EnemyBullet();
		straightBullet[i]->Initialize();
	}
	//ホーミング_2
	for (int i = 0; i < HOMING_MAX; i++)
	{
		homingBullet_2[i] = new EnemyBullet();
		homingBullet_2[i]->Initialize();
	}
}

void Enemy::Init()
{
	//行動に移すまでの時間
	maxMoveTime = 1.8f;
	speed = 0.5f;
	HP = maxHP;
	position = { 0,0,-5.0f };
	Angle = 0;
	targetTime = 0;
	targetFlag = false;
	//突撃
	preAssaultTime = 0;
	bfoAssaultTime = 0;
	playerOldPos = { 0,0,0 };
	oldPos = { 0,0,0 };
	//弾
	bulletAngle = 0;
	//
	isHoming = false;
	boundFlag = false;
	isCircle = false;
	isMove = false;
	moveTime = 0;
	phase_ = Phase::WALK;
	state_ = WALK;
	isColl = false;
	isDeath = false;
	isBall = false;
	halfCount = 0;
	stCount = 0;
	for (int i = 0; i < BULLET_MAX; i++)
	{
		homingBullet->SetIsAlive(true, i);
		boundBullet->SetIsAlive(true, i);
		for (int j = 0; j < CIRCLE_MAX; j++)
		{
			CircleBullet[j]->SetEnemyBulletPos({0,0,10}, i);
		}
		
		for (int j = 0; j < COUNTER_MAX; j++)
		{
			flowerBullet[j]->SetIsAlive(true, i);
		}
	}
}

void Enemy::ResourceUpdate()
{
	fbxEnemy->Update();
}

void Enemy::Update(Player* player, DirectXCommon* dxCommon, Camera* camera)
{
	Move(player);
	//エフェクト
	effects->Load(L"effectsTest/10/block.efk");
	effects->Play();
	effects->SetPosition({ position.x,position.y,position.z });
	effects->SetScale({ 0.2f,0.2f,0.2f });
	effects->SetRotation({ 0,180,0 });
	effects->SetSpeed(5);
	effects->Update(dxCommon->GetCmdList(), camera);
	//HPバーのサイズ
	SetHpSprite();
	//FBX切り替え
	switch (state_)
	{
	case Enemy::ASSALT:
		fbxEnemy->PlayAnimation(fbxEnemy->GetArmature("run"));
		fbxEnemy->SetArmatureNo(fbxEnemy->GetArmature("run"));
		break;
	case Enemy::DAMAGE:
		fbxEnemy->PlayAnimation(fbxEnemy->GetArmature("damage"));
		fbxEnemy->SetArmatureNo(fbxEnemy->GetArmature("damage"));
		break;
	case Enemy::ATTACK:
		fbxEnemy->PlayAnimation(fbxEnemy->GetArmature("attack"));
		fbxEnemy->SetArmatureNo(fbxEnemy->GetArmature("attack"));
		break;
	case Enemy::DEATH:
		fbxEnemy->PlayAnimation(fbxEnemy->GetArmature("death"));
		fbxEnemy->SetArmatureNo(fbxEnemy->GetArmature("death"));
		break;
	case Enemy::JAMP:
		fbxEnemy->PlayAnimation(fbxEnemy->GetArmature("jamp"));
		fbxEnemy->SetArmatureNo(fbxEnemy->GetArmature("jamp"));
		break;
	case Enemy::WALK:
		fbxEnemy->PlayAnimation(fbxEnemy->GetArmature("walk"));
		fbxEnemy->SetArmatureNo(fbxEnemy->GetArmature("walk"));
		break;
	default:
		break;
	}

	spineTrans = fbxEnemy->GetMatNowPose(fbxEnemy->GetBoneName("mixamorig:Hips"));
	spineWorld = fbxEnemy->GetMatWorld();
	spineMat = spineTrans * spineWorld;

	fbxEnemy->SetPosition(position);

	for (int i = 0; i < COUNTER_MAX; i++)
	{
		flowerBullet[i]->Update();
	}
	boundBullet->Update();
	homingBullet->Update();
	for (int i = 0; i < CIRCLE_MAX; i++)
	{
		CircleBullet[i]->Update();
	}
	for (int i = 0; i < STRAIGHT_MAX; i++)
	{
		straightBullet[i]->Update();
	}
	for (int i = 0; i < HOMING_MAX; i++)
	{
		homingBullet_2[i]->Update();
	}
}

void Enemy::Move(Player* player)
{
	//一定時間になったらプレイヤーとの距離を取得
	if (maxMoveTime < moveTime)
	{
		//HPがある値になったら特定の攻撃をする
		if (HP <= maxHP / 2 && HP > 3 && halfCount <= 0)
		{
			isDecrease = true;
			difference = -1;
		}
		else if (HP <= 3 && hp2Count <= 0)
		{
			difference = -2;
		}
		else
		{
			isMove = true;
			moveTime = maxMoveTime;
			difference = calculation->Difference(position, player->GetPosition());
		}
	
	}
	else if (maxMoveTime >= moveTime && isMove == false)
	{
		moveTime += 0.1f;
		Target(player);
		phase_ = Phase::WALK;
		bulletAngle = 0;
		HBRand = calculation->Get_rand_range(1, 2);
	}

	//攻撃切り替え プレイヤーとの距離で判断
	const float attack = 5.0f;
	const float bullet = 9.0f;
	const float half = -1.0f;
	const float little = -2.0f;
	if (isMove)
	{
		moveTime = 0;
		
		if (difference >= 0 && difference <= attack)
		{
			phase_ = Phase::ATTACK;
			maxMoveTime = 1.8f;
		}
		else if (difference > attack && difference <= bullet)
		{
			if (HBRand == 1)
			{
				if (HP <= maxHP / 2)
				{
					phase_ = Phase::HOMING_2;
					maxMoveTime = 2.2f;
				}
				else
				{
					phase_ = Phase::HOMIG;
					maxMoveTime = 1.8f;
				}
				
			}
			else
			{
				phase_ = Phase::BALL;
				maxMoveTime = 3.0f;
			}
			
		}
		else if (difference > bullet)
		{
			phase_ = Phase::ASSALT;
			maxMoveTime = 1.8f;
		}
		else if (difference == half)
		{
			phase_ = Phase::CIRCLE;
			maxMoveTime = 2.5f;
		}
		else if (difference == little)
		{
			phase_ = Phase::STRAIGHT;
			maxMoveTime = 1.8f;
		}
	}
	Assault(player);
	EnemyCircle();
	BoundBullet(player);
	HomingBullet(player);
	Attack(player);
	Damage(player);
	PlayerenemyCollision(player);
	CheckAllCollision(player);
	BallBullet();
	Death();
	StraightBullet();
	Homing_2_Bullet(player);
}

void Enemy::Target(Player* player)
{
	//プレイヤーの方に向く処理
	XMFLOAT3 playerPos = player->GetPosition();
	const float direction = 10.0f;
	float AngleX = position.x - playerPos.x;
	float AngleZ = position.z - playerPos.z;
	Angle = (atan2(AngleX, AngleZ)) * angle_halfCircle / PI + direction;

	fbxEnemy->SetRotation({ 0,angle_halfCircle + Angle,0 });
}

void Enemy::Assault(Player* player)
{
	if (phase_ == Phase::ASSALT)
	{
		//予測動作
		preAssaultTime++;
		moveTime = 0;
		state_ = ASSALT;
		if (preAssaultTime < maxPreAssaultTime)
		{
			//アニメーションストップ
			FbxStop();
		}
	}
	//静止時間が一定時間を達したら突進するための情報を取得
	if (preAssaultTime >= maxPreAssaultTime && assaultFlag == false)
	{
		oldPos = position;
		pOldPos = { player->GetPosition().x, 0, player->GetPosition().z };
		XMVECTOR forwardVec = calculation->ForwardVector(1.5f, position, player->GetPosition());
		playerOldPos = calculation->AddVec(pOldPos, -forwardVec);
		
		assaultFlag = true;
		bfoAssaultTime = 0;
	}
	//突進開始
	if (assaultFlag)
	{
		SetFbxTime();
		const float frame = 0.05f;
		bfoAssaultTime += frame;
		position = easing->ease(oldPos, playerOldPos, bfoAssaultTime);

		if (bfoAssaultTime >= easing->maxflame)
		{
			preAssaultTime = 0;
			assaultFlag = false;
			isMove = false;
			state_ = WALK;
			phase_ = Phase::WALK;
		}
	}

	fbxEnemy->SetPosition(position);
}

void Enemy::HomingBullet(Player* player)
{
	if (isHoming == false && isMove && phase_ == Phase::HOMIG)
	{
		HStop = true;
		state_ = WALK;
	}
	//予測動作
	if (HStop)
	{
		//アニメーションストップ
		FbxStop();
		if (--StopTimer_ < 0)
		{
			HStop = false;
			moveTime = 0;
			isHoming = true;
		}
	}

	if (isHoming)
	{
		//弾の処理
		if (homingTime >= maxHomingTime)
		{
			if (homingBullet->GetIsAlive(bulletCount) == true)
			{
				//広がる範囲をランダムで求める
				randX = calculation->Get_rand_range(-35, 70);
				float disX;
				disX = randX * 0.1f;
				XMVECTOR v0;
				v0 = { disX, 0,0,0 };

				//弾のスピード計算
				XMFLOAT3 v;
				v = calculation->RotationYVelocity(v0, bulletAngle);

				//回転後のスピード
				XMFLOAT3 velocity;
				velocity = { v.x,0.05f,v.z };

				// ボーンの計算済みワールド行列（ボーンのローカル行列×プレイヤーのワールド行列）
				XMFLOAT3 pos = calculation->GetBonePosition("mixamorig:HeadTop_End", fbxEnemy);
				//開始点
				point.p0 = pos;
				//第1ポイント
				point.p1 = {
					pos.x + v.x,
					pos.y - 0.5f + v.y,
					pos.z - 0.5f + v.z };
				//第2ポイント
				point.p2 = {
						player->GetPosition().x,
						player->GetPosition().y + 0.5f,
						player->GetPosition().z };
				//終了点
				point.p3 = { point.p2.x + v.x, v.y, point.p2.z - 0.3f + v.z };
				//弾の生成、初期化
				homingBullet->HomingInit(pos, point.p0, point.p1, point.p2, {0.05f,0,0}, bulletCount);
				homingBullet->SetIsAlive(false, bulletCount);
				homingBulletFlag[bulletCount] = true;
				homingTime = 0;
				bulletCount = BulletCount(bulletCount);
			}
		}
		else
		{
			homingTime += 0.3f;
		}

		if (--hStartTimer_ <= 0)
		{
			//攻撃時間が一定時間を達したら攻撃を終了させる
			isHoming = false;
			isMove = false;
			StopTimer_ = maxStopTime;
			phase_ = Phase::WALK;
			//弾がまだ更新されていたらステージ外に出す
			if (homingBullet->GetIsAlive(bulletCount) == false)
			{
				for (int i = 0; i < BULLET_MAX; i++)
				{
					homingBullet->SetIsAlive(true, i);
				}
			}
		}
	}
	else
	{
		//タイマーの初期化
		hStartTimer_ = hEndTime;
	}

	for (int i = 0; i < BULLET_MAX; i++)
	{
		if (homingBulletFlag[i] && homingBullet->GetIsAlive(i) == false)
		{
			//弾の更新
			homingBullet->Homing(easing, i);
			//生成されていなかったらフラグをfalse
			if (homingBullet->GetIsAlive(i))
			{
				homingBulletFlag[i] = false;
			}
		}
	}
}

void Enemy::BoundBullet(Player* player)
{
	const float frame = 0.1f;
	if (isColl)
	{
		//プレイヤーの攻撃が当たったら
		assaultFlag = false;
		isMove = true;
		state_ = JAMP;
	}
	else
	{
		fbxBoundTime = fbxEnemy->GetStartTime();
	}

	//fbxアニメーションの処理
	if (state_ == JAMP)
	{
		const int setFrame = 36;
		//fbxの時間がmaxtimeに達したら移動する
		if (fbxBoundTime > fbxEnemy->GetFrame() * setFrame)
		{
			fbxBoundTime += fbxEnemy->GetFrame();
			phase_ = Phase::Bound;
		}
		else
		{
			//移動する前までfbxの時間を倍にする
			FbxTime frame = fbxEnemy->GetFrame() * 2;
			fbxBoundTime += frame;
		}
		fbxEnemy->SetNowTime(fbxBoundTime);
	}

	if (phase_ == Phase::Bound)
	{
		//敵の移動処理
		const float kBulletSpeedY = 1.0f;
		float speedY = kBulletSpeedY;
		float dy = speedY * boundTime;
		//前方ベクトルを求める
		oldPos = position;
		XMVECTOR forwardVec = calculation->ForwardVector(0.8f, position, player->GetPosition());
		//現在の座標の前方ベクトル分後ろを求める
		XMFLOAT3 futurePos = calculation->AddVec(position, forwardVec);
		//x,z座標を求める
		boundeaseTime += 0.01f;
		position = easing->EaseSine_out(oldPos, futurePos, boundeaseTime);
		//y座標は軌道
		position.y = dy * sin((PI / angle_halfCircle) * boundAngle) - (0.5f * (gravity * (boundTime * boundTime)));
		boundTime += frame;

		if (boundeaseTime >= easing->maxflame)
		{
			boundeaseTime = easing->maxflame;
		}

		isBoundShot = true;
	}

	if (isBoundShot)
	{
		boundShotTime += frame;

		if (boundShotTime >= maxBoundTime)
		{
			if (boundBullet->GetIsAlive(boundCount) == true)
			{
				//弾の処理
				// ボーンの計算済みワールド行列（ボーンのローカル行列×プレイヤーのワールド行列）
				XMFLOAT3 bulletPos;
				bulletPos = calculation->GetBonePosition("mixamorig:HeadTop_End", fbxEnemy);

				//弾の生成、初期化
				boundBullet->Init(bulletPos, oldPos, { 0.1f,0.1f,0.1f }, boundCount);
				boundBullet->SetIsAlive(false, boundCount);
				boundShotTime = 0;
				boundCount = BulletCount(boundCount);
			}
		}
		
		if (position.y <= 0 && boundTime > frame)
		{
			isBoundShot = false;
			isMove = false;
			isColl = false;
			state_ = WALK;
			phase_ = Phase::WALK;
			boundTime = 0;
		}
	}

	for (int i = 0; i < BULLET_MAX; i++)
	{
		if (boundBullet->GetIsAlive(i) == false)
		{
			boundBullet->BoundBullet(easing, i);
		}
	}
	fbxEnemy->SetPosition(position);

}

float Enemy::BallAngle(XMVECTOR bDis, XMVECTOR cDis, const int maxBall, int num)
{
	ballDis = bDis;
	this->cDis = cDis;
	int a = num % maxBall;
	float ballAngle = 0;
	ballAngle = PI / angle_halfCircle * ((int)(angle_halfCircle * 2 / maxBall * (float)a) % (int)angle_halfCircle * 2);
	return ballAngle;
}

void Enemy::BallBullet()
{
	if (isBall == false && isMove && phase_ == Phase::BALL)
	{
		BStop = true;
		state_ = WALK;
	}

	if (BStop)
	{
		//アニメーションストップ
		FbxStop();
		if (--StopTimer_ < 0)
		{
			BStop = false;
			moveTime = 0;
			isBall = true;
			StopTimer_ = maxStopTime;
		}
	}

	if (isBall)
	{
		const float frame = 0.5f;
		//弾の処理
		ballTime += frame;

		const float maxTime = 10;
		if (ballTime >= maxTime)
		{
			for (int j = 0; j < BULLET_MAX; j++)
			{
				ballDis = {0,0,0};

				//一つの輪の弾の数
				const int circle_1 = 13;
				const int circle_2 = 8;
				const int circle_3 = 4;

				//y軸まわりに回転させるためのangle計算
				float ballAngle = 0;
				if (j < circle_1)
				{
					ballAngle = BallAngle({ 0.3f,1,0 }, { 0,0,1.5f,0 }, circle_1, j);
				}
				else if (j >= circle_1 && j < circle_1 * 2)
				{
					ballAngle = BallAngle({ -0.3f,1,0 }, { 0,0,1.5f,0 }, circle_1, j);
				}
				else if (j >= circle_1 * 2 && j < circle_1 * 2 + circle_2)
				{
					ballAngle = BallAngle({ 1.0f,1,0 }, { 0,0,0.8f }, circle_2, (j - circle_1 * 2));
				}
				else if (j >= circle_1 * 2 + circle_2 && j < circle_1 * 2 + circle_2 * 2)
				{
					ballAngle = BallAngle({ -1.0f,1,0 }, { 0,0,0.8f,0 }, circle_2, (j - circle_1 * 2));
				}
				else if (j >= circle_1 * 2 + circle_2 * 2 && j < circle_1 * 2 + circle_2 * 2 + circle_3)
				{
					ballAngle = BallAngle({ 1.8f,1,0 }, { 0,0,0.2f }, circle_3, (j - circle_1 * 2 + circle_2 * 2));
				}
				else if (j >= circle_1 * 2 + circle_2 * 2 + circle_3 && j < circle_1 * 2 + circle_2 * 2 + circle_3 * 2)
				{
					ballAngle = BallAngle({ -1.8f,1,0 }, { 0,0,0.2f }, circle_3, (j - circle_1 * 2 + circle_2 * 2));
				}

				if (j == BULLET_MAX - 1)
				{
					//弾がmax個移動したら次の弾に変える
					num++;

					if (num == COUNTER_MAX)
					{
						num = 0;
						count++;
					}
				}

				//回転後の速度
				XMFLOAT3 v;
				v = calculation->RotationYVelocity(ballDis, 0);
				//現在の位置に回転後の速度を足す
				proPos = {
					position.x + v.x,
					position.y + v.y + 0.3f,
					position.z + v.z
				};
				//初期化
				flowerBullet[num]->FlowerInit(proPos, { 0.07f,0.1f,0.1f }, cDis, proPos, ballAngle, j);
				flowerBullet[num]->SetIsAlive(false, j);
			}
			ballTime = 0;
		}

		if (count > COUNTER_MAX)
		{
			phase_ = Phase::WALK;
			isBall = false;
			isMove = false;
			ballTime = 0;
			num = 0;
		}
	}
	else
	{
		count = 0;
		for (int i = 0; i < COUNTER_MAX; i++)
		{
			if (flowerBullet[i]->GetIsAlive(i) == false)
			{
				flowerBullet[i]->SetIsAlive(true, i);
			}
		}
	}
	//弾の移動
	for (int i = 0; i < COUNTER_MAX; i++)
	{
		flowerBullet[i]->FlowerBullet(position);
	}
	
}

void Enemy::Damage(Player* player)
{
	if (isDecrease)
	{
		state_ = DAMAGE;
		assaultFlag = false;
		isBound = false;
		isMove = false;
	}

	if (state_ == DAMAGE)
	{
		//ダメージのアニメーション処理
		if (fbxEnemy->GetNowTime() >= fbxEnemy->GetEndTime())
		{
			isDecrease = false;
			isMove = true;
			moveTime = maxMoveTime;
			state_ = WALK;
		}
	}
}

void Enemy::Attack(Player* player)
{
	if (phase_ == Phase::ATTACK)
	{
		state_ = ATTACK;
	}

	if (state_ == ATTACK)
	{
		//前方ベクトルと移動後の座標を求める
		oldPos = position;
		XMVECTOR forwardVec = calculation->ForwardVector(0.8f, position, player->GetPosition());
	    XMFLOAT3 futurePos = calculation->AddVec(position, -forwardVec);
		//移動後の座標へ移動
		const float frame = 0.1f;
		attackEaseTime += frame;
		position = easing->ease(oldPos, futurePos, attackEaseTime);
		//イージングの時間が一定時間を越したら
		if (attackEaseTime >= easing->maxflame)
		{
			position = oldPos;
		}
		//fbxがend時間までいったら行動終わり
		if (fbxEnemy->GetNowTime() >= fbxEnemy->GetEndTime())
		{
			isMove = false;
			state_ = WALK;
			phase_ = Phase::WALK;
		}
	}
	else
	{
		attackEaseTime = 0;
	}

	fbxEnemy->SetPosition(position);
}

void Enemy::Death()
{
	if (HP <= 0)
	{
		isColl = false;
		isDecrease = false;
		isBound = false;
		isMove = false;
		moveTime = 0;
		state_ = DEATH;
	}

	if (state_ == DEATH)
	{
		if (fbxEnemy->GetNowTime() >= fbxEnemy->GetEndTime())
		{
			isDeath = true;
			state_ = WALK;
		}
	}
}

void Enemy::CircleBalletInit(EnemyBullet* bullet, float angle, int count)
{
	//HP / 2の時に出る攻撃の弾の初期化
	if (bullet->GetIsAlive(count) == true)
	{
		const float kBulletSpeed = 0.1f;
		XMVECTOR distance;
		distance = { 0,0,kBulletSpeed };
		//弾のスピード計算
		XMFLOAT3 v;
		v = calculation->RotationYVelocity(distance, angle);

		//回転後のスピード
		XMFLOAT3 velocity;
		velocity = { v.x,0.05f,v.z };

		// ボーンの計算済みワールド行列（ボーンのローカル行列×プレイヤーのワールド行列）
		XMFLOAT3 bulletPos = calculation->GetBonePosition("mixamorig:HeadTop_End", fbxEnemy);

		//弾を生成し、初期化
		bullet->Init(bulletPos, { 0,0,0 }, velocity, count);
		bullet->SetIsAlive(false, count);
	}
}

void Enemy::EnemyCircle()
{
	if (phase_ == Phase::CIRCLE)
	{
		isColl = false;
		const float angleFrame = 1.0f;
		const float frame = 0.3f;
		bulletAngle += angleFrame;
		circleTime += frame;

		if (circleTime >= maxCircleTime)
		{
			//一定時間に達したら弾の初期化
			const float maxCircle = 3;
			const float addAngle = (angle_halfCircle * 2) / maxCircle;
			for (int i = 0; i < CIRCLE_MAX; i++)
			{
				CircleBalletInit(CircleBullet[i], bulletAngle + addAngle * i, circleCount);
			}

			circleTime = 0;
			circleCount = BulletCount(circleCount);
		}
		halfCount++;
	}
	else
	{
		bulletAngle = 0;
	}

	const float maxAngle = 270.0f;
	if (bulletAngle >= maxAngle)
	{
		phase_ = Phase::WALK;
		isMove = false;
		circleTime = 0;
	}

	for (int i = 0; i < BULLET_MAX; i++)
	{
		for (int j = 0; j < CIRCLE_MAX; j++)
		{
			if (CircleBullet[j]->GetIsAlive(i) == false)
			{
				CircleBullet[j]->CircleBullet(i);
			}
		}
	}
}

void Enemy::RLMove(XMVECTOR lim, float initAngle)
{
	float limit = initAngle;
	// 軸単位で動かすかどうか
	const float isMove = (lim.m128_f32[0] ? 1.0f : 0.0f);
	float speed = 30.0f;
	if (isChange)
	{
		// +方向に移動する
		limit += lim.m128_f32[0];
		stAngle += isMove * speed;

		if ((isMove && (stAngle >= limit)))
		{
			isChange = false;
		}
	}
	else
	{
		// -方向に移動する
		limit -= lim.m128_f32[0];
		stAngle -= isMove * speed;

		if ((isMove && (stAngle <= limit)))
		{
			isChange = true;
		}
	}
}

void Enemy::StraightBullet()
{
	if (isSt == false && isMove && phase_ == Phase::STRAIGHT)
	{
		isStStpot = true;
		state_ = WALK;
	}
	//予測動作
	if (isStStpot)
	{
		isColl = false;
		//アニメーションストップ
		FbxStop();
		if (--StopTimer_ < 0)
		{
			isStStpot = false;
			moveTime = 0;
			isSt = true;
		}
	}

	if (isSt)
	{
		isColl = false;
		//弾の処理
		const float maxTime = 10;
		if (stTime >= maxTime)
		{
			for (int i = 0; i < STRAIGHT_MAX - 1; i++)
			{
				float fixationAngle = angle_halfCircle / (STRAIGHT_MAX - 2);
				float a = (Angle)+(fixationAngle * i);

				//回転後の速度
				XMFLOAT3 v;
				v = calculation->RotationYVelocity({ 0.1f,0,0 }, a);
				//現在の位置に回転後の速度を足す
				XMFLOAT3 bulletPos = position;
				//初期化
				straightBullet[i]->Init(bulletPos, { 0,0,0 }, v, stCount);
				straightBullet[i]->SetIsAlive(false, stCount);

			}
			//回転後の速度
			RLMove({ 60,0,0 }, 0);
			XMFLOAT3 v;
			//ずっと取り続けてるからあとで直す
			float a = angle_halfCircle / 2 + Angle + stAngle;
			v = calculation->RotationYVelocity({ 0.1f,0,0 }, a);
			//現在の位置に回転後の速度を足す
			XMFLOAT3 bulletPos = position;
			//初期化
			straightBullet[STRAIGHT_MAX - 1]->Init(bulletPos, { 0,0,0 }, v, stCount);
			straightBullet[STRAIGHT_MAX - 1]->SetIsAlive(false, stCount);
			stCount = BulletCount(stCount);
			stTime = 0;
		}
		else
		{
			const float frame = 0.5f;
			stTime += frame;
		}

		if (--stStartTimer_ <= 0)
		{
			//攻撃時間が一定時間を達したら攻撃を終了させる
			isSt = false;
			isMove = false;
			StopTimer_ = maxStopTime;
			phase_ = Phase::WALK;
			hp2Count++;
		}
	}
	else
	{
		stStartTimer_ = stEndTime;
	}
	

	for (int i = 0; i < BULLET_MAX; i++)
	{
		for (int j = 0; j < STRAIGHT_MAX; j++)
		{
			if (straightBullet[j]->GetIsAlive(i) == false)
			{
				straightBullet[j]->StraightBullet(i);
			}
		}
	}
}

void Enemy::Homing_2_Init(Player *player, Point point)
{
	// ボーンの計算済みワールド行列（ボーンのローカル行列×プレイヤーのワールド行列）
	XMFLOAT3 pos = calculation->GetBonePosition("mixamorig:HeadTop_End", fbxEnemy);
	//p1の中心座標からの距離設定
	XMVECTOR distance;
	distance = { 0.5f, 0, 0 };
	float angle = angle_halfCircle * 2 / HOMING_MAX;
	for (int i = 0; i < HOMING_MAX; i++)
	{
		/*-----------------開始点-----------------*/
		point_2[i].p0 = pos;
		/*-----------------第1ポイント-----------------*/
		//開始点からspeed分移動した後の座標
		XMFLOAT3 p1[HOMING_MAX];
		//angle分Z回転簿の座標
		XMFLOAT3 v;
		v = calculation->RotationYVelocity(distance, Angle);
		v.y = 0.5f;
		XMFLOAT3 center = { point_2[i].p0.x + v.x, point_2[i].p0.y + v.y, point_2[i].p0.z + v.z };
		XMVECTOR dis_p1 = { 0,5.0f,0 };
		p1[i] = calculation->RotationZVelocity(dis_p1, Angle + angle * i);
		point_2[i].p1 = { center.x + p1[i].x, center.y + p1[i].y, center.z + p1[i].z };
		/*-----------------第2ポイント-----------------*/
		point_2[i].p2 = {
				player->GetPosition().x,
				player->GetPosition().y + v.y,
				player->GetPosition().z };
		/*-----------------終了点-----------------*/
		//p2点からspeed分移動した後の座標
		XMVECTOR vec_p3 = calculation->ForwardVector(0.3f, pos, player->GetPosition());
		XMFLOAT3 p3_center = calculation->AddVec(pos, -vec_p3);
		//第1ポイント
		XMFLOAT3 p3[HOMING_MAX];
		//angle分Z回転簿の座標
		p3[i] = calculation->RotationZVelocity(distance, angle * (i + 1));
		point_2[i].p3 = p3[i];
	}
	
}

void Enemy::Homing_2_Bullet(Player* player)
{
	if (isHoming_2 == false && isMove && phase_ == Phase::HOMING_2)
	{
		H2Stop = true;
		state_ = WALK;
	}
	//予測動作
	if (H2Stop)
	{
		//アニメーションストップ
		FbxStop();
		if (--StopTimer_ < 0)
		{
			H2Stop = false;
			moveTime = 0;
			isHoming_2 = true;
		}
	}

	if (isHoming_2)
	{
		const float maxTime = 1.0f;
		//弾の処理
		if (homingTime_2 >= maxTime)
		{
			for (int i = 0; i < HOMING_MAX; i++)
			{
				if (homingBullet_2[i]->GetIsAlive(bulletCount) == true)
				{
					//ポイントの初期化
					Homing_2_Init(player, point);
					// ボーンの計算済みワールド行列（ボーンのローカル行列×プレイヤーのワールド行列）
					XMFLOAT3 pos = calculation->GetBonePosition("mixamorig:HeadTop_End", fbxEnemy);
					//弾の生成、初期化
					homingBullet_2[i]->HomingInit(pos, point_2[i].p0, point_2[i].p1, point_2[i].p2, { 0.01f,0,0 }, bulletCount);
					homingBullet_2[i]->SetIsAlive(false, bulletCount);
					homingTime_2 = 0;
					bulletCount = BulletCount(bulletCount);
				}
			}
		}
		else
		{
			const float frame = 0.1f;
			homingTime_2 += frame;
		}

		if (--h2StartTimer_ <= 0)
		{
			//攻撃時間が一定時間を達したら攻撃を終了させる
			isHoming_2 = false;
			isMove = false;
			StopTimer_ = maxStopTime;
			phase_ = Phase::WALK;
		}
	}
	else
	{
		//タイマーの初期化
		h2StartTimer_ = hEndTime;
	}
	

	for (int i = 0; i < HOMING_MAX; i++)
	{
		for (int j = 0; j < BULLET_MAX; j++)
		{
			if (homingBullet_2[i]->GetIsAlive(j) == false)
			{
				//弾の更新
				homingBullet_2[i]->Homing(easing, j);

			}
		}
	}
}

int Enemy::BulletCount(int count)
{
	count++;
	if (count >= BULLET_MAX)
	{
		count = 0;
	}
	return count;
}

void Enemy::Draw(DirectXCommon* dxCommon)
{
	effects->Draw(dxCommon->GetCmdList());
	fbxEnemy->Draw(dxCommon->GetCmdList());
}

void Enemy::PostEffectDraw()
{
	if (!isDeath)
	{
		for (int i = 0; i < COUNTER_MAX; i++)
		{
			flowerBullet[i]->Draw();
		}
		boundBullet->Draw();
		homingBullet->Draw();

		for (int i = 0; i < CIRCLE_MAX; i++)
		{
			CircleBullet[i]->Draw();
		}
		for (int i = 0; i < STRAIGHT_MAX; i++)
		{
			straightBullet[i]->Draw();
		}
		for (int i = 0; i < HOMING_MAX; i++)
		{
			homingBullet_2[i]->Draw();
		}
	}
}

void Enemy::DrawSprite()
{
	HPBar->Draw();
}

void Enemy::PlayerenemyCollision(Player* player)
{
	//敵の当たり判定の座標
	if (assaultFlag)
	{
		enemySphere[0].center = XMLoadFloat3(&calculation->GetBonePosition("mixamorig:RightUpLeg", fbxEnemy));
		enemySphere[1].center = XMLoadFloat3(&calculation->GetBonePosition("mixamorig:RightUpLeg", fbxEnemy));
	}
	else
	{
		enemySphere[0].center = XMLoadFloat3(&calculation->GetBonePosition("mixamorig:RightHand", fbxEnemy));
		enemySphere[1].center = XMLoadFloat3(&calculation->GetBonePosition("mixamorig:LeftHand", fbxEnemy));
		
	}
	//敵の当たり判定の半径
	enemySphere[0].radius = enemyRad;
	enemySphere[1].radius = enemyRad;
	//プレイヤーの当たり判定の位置
	playerSphere.center = { player->GetPosition().x, player->GetPosition().y + 1.0f, player->GetPosition().z };
	playerSphere.radius = playerRad;

	isHit_Right = Collision::CheckSphere2(playerSphere, enemySphere[0]);
	isHit_Left = Collision::CheckSphere2(playerSphere, enemySphere[1]);
	//当たった時の処理
	if (player->GetState() != Player::DODGE)
	{
		if (state_ == ASSALT)
		{
			if (isHit_Left && Hit == false)
			{
				player->SetHP(player->GetHP() - 1);
				player->fbxPlayer->SetNowTime(player->fbxPlayer->GetStartTime());
				Hit = true;
				player->SetIsKnock(true);
			}
		}
		else if (state_ == ATTACK)
		{
			if (isHit_Right && Hit == false || isHit_Left && Hit == false || isHit_Right && isHit_Left && Hit == false)
			{
				player->SetHP(player->GetHP() - 1);
				player->fbxPlayer->SetNowTime(player->fbxPlayer->GetStartTime());
				Hit = true;
				player->SetIsKnock(true);
			}
		}
		else
		{
			Hit = false;
		}
	}
}

void Enemy::BulletCollision(Player* player, EnemyBullet* bullet)
{
	//バウンド弾とプレイヤーの当たり判定
	for (int i = 0; i < BULLET_MAX; i++)
	{
		if (bullet->GetIsAlive(i) == false)
		{
			Sphere posBP, posBB;
			posBP.center = { player->GetPosition().x, player->GetPosition().y, player->GetPosition().z };
			posBP.radius = 0.5f;

			posBB.center = { bullet->GetEnemyBulletPos(i).x, bullet->GetEnemyBulletPos(i).y, bullet->GetEnemyBulletPos(i).z };
			posBB.radius = 0.25f;
			if (Collision::CheckSphere2(posBP, posBB))
			{
				bullet->OnCollision(i);
				player->SetHP(player->GetHP() - 1);
			}
		}
	}
}

void Enemy::CheckAllCollision(Player* player)
{
	if (player->GetState() != Player::DODGE || HP > 0)
	{
		//バウンド弾とプレイヤーの当たり判定
		BulletCollision(player, boundBullet);

		//サークル弾とプレイヤーの当たり判定
		for (int i = 0; i < CIRCLE_MAX; i++)
		{
			BulletCollision(player, CircleBullet[i]);
		}

		//ホーミング弾とプレイヤーの当たり判定
		BulletCollision(player, homingBullet);

		//球弾とプレイヤーの当たり判定
		for (int i = 0; i < COUNTER_MAX; i++)
		{
			BulletCollision(player, flowerBullet[i]);
		}

		//直進弾とプレイヤーの当たり判定
		for (int i = 0; i < STRAIGHT_MAX; i++)
		{
			BulletCollision(player, straightBullet[i]);
		}

		//ホーミング2とプレイヤーの当たり判定
		for (int i = 0; i < HOMING_MAX; i++)
		{
			BulletCollision(player, homingBullet_2[i]);
		}
	}
}

void Enemy::SetHpSprite()
{
	float sizeX = 679.0f;
	float hpSize = (sizeX / maxHP) * HP;
	if (HP < 0) { HP = 0; }
	HPBar->SetSize({ hpSize, 22.0f });
}

void Enemy::FbxStop()
{
	animeTime = fbxEnemy->GetStartTime();
	fbxEnemy->SetNowTime(animeTime);
}

void Enemy::SetFbxTime()
{
	animeTime = fbxEnemy->GetNowTime() + fbxEnemy->GetFrame();
	fbxEnemy->SetNowTime(animeTime);
}
