﻿#include "Player.h"
#include <time.h>
#include <cassert>
#include "SphereCollider.h"
#include "Input.h"

using namespace DirectX;

void Player::Initialize(DirectXCommon* dxCommon, Camera* camera)
{
	player_FbxModel = FbxLoader::GetInstance()->LoadMadelFromFile("player");
	fbxPlayer = new FbxObject3d;
	fbxPlayer->Initialize();
	fbxPlayer->SetModel(player_FbxModel.get());
	//再生アニメーション
	fbxPlayer->LoadAnumation();
	fbxPlayer->SetScale({ 0.01f,0.01f,0.01f });
	fbxPlayer->SetPosition({ 0,0, 5.5f });

	Sprite::LoadTexture(4, L"Resource/playerHP_1.png");

	HPBar = Sprite::Create(4, { 372.0f, 629.0f });

	easing = new Easing();
	easing->Initialize();

	effects = new Effects();
	effects->Initialize(dxCommon->GetDev(), dxCommon->GetCmdQueue(), camera);
}

void Player::Init()
{
	HP = maxHP;
	isKnock = false;
	isWalk = false;
	AnimationTime = 0;
	position = { 0,0.3f,-15.0f };
	isDeath = false;
	state_ = WAIT;
}

void Player::ResourceUpdate()
{
	fbxPlayer->Update();
}

void Player::Update(DirectXCommon* dxCommon, Camera* camera, XMFLOAT3 enemyPos)
{
	switch (state_)
	{
	case Player::MOVE:
		fbxPlayer->PlayAnimation(fbxPlayer->GetArmature("run"));
		fbxPlayer->SetArmatureNo(fbxPlayer->GetArmature("run"));

		//エフェクト再生
		effects->Load(L"effectsTest/10/boneWalk.efk");
		effects->Play();
		effects->SetPosition(position);
		effects->SetScale({ 0.1f,0.1f,0.1f });
		effects->SetSpeed(5);
		effects->Update(dxCommon->GetCmdList(), camera);
		break;
	case Player::ATTACK: 
		fbxPlayer->PlayAnimation(fbxPlayer->GetArmature("comboAttack"));
		fbxPlayer->SetArmatureNo(fbxPlayer->GetArmature("comboAttack"));
		break;
	case Player::ATTACK_2:
		fbxPlayer->PlayAnimation(fbxPlayer->GetArmature("comboAttack"));
		fbxPlayer->SetArmatureNo(fbxPlayer->GetArmature("comboAttack"));
		break;
	case Player::ATTACK_3:
		fbxPlayer->PlayAnimation(fbxPlayer->GetArmature("comboAttack"));
		fbxPlayer->SetArmatureNo(fbxPlayer->GetArmature("comboAttack"));
		break;
	case Player::DODGE:
		fbxPlayer->PlayAnimation(fbxPlayer->GetArmature("dodge"));
		fbxPlayer->SetArmatureNo(fbxPlayer->GetArmature("dodge"));
		break;
	case Player::DAMAGE:
		fbxPlayer->PlayAnimation(fbxPlayer->GetArmature("damage"));
		fbxPlayer->SetArmatureNo(fbxPlayer->GetArmature("damage"));
		break;
	case Player::DEATH:
		fbxPlayer->PlayAnimation(fbxPlayer->GetArmature("death"));
		fbxPlayer->SetArmatureNo(fbxPlayer->GetArmature("death"));
		break;
	default:
		fbxPlayer->PlayAnimation(fbxPlayer->GetArmature("wait"));
		fbxPlayer->SetArmatureNo(fbxPlayer->GetArmature("wait"));
		break;
	}

	//ボーンのワールド行列取得
	matWorld = fbxPlayer->GetMatWorld();

	Move(camera, enemyPos);
	knockBack(dxCommon, camera, enemyPos);
	Death();
	Attack(enemyPos);
	Dodge(enemyPos);

	//HPバーのサイズ変更
	SetHpSprite();
	
}

void Player::SetMove(XMVECTOR moveVec, XMVECTOR rotVec)
{
	//移動後の座標 = 移動前の座標 ＋ 速度ベクトル
	position = calculation->AddVec(position, moveVec);
	isWalk = true;
	//移動後の座標 = 移動前の座標 ＋ 速度ベクトル
	rotPos = calculation->AddVec(position, rotVec);
	state_ = MOVE;
}

void Player::Move(Camera* camera, XMFLOAT3 enemyPos)
{
	///Todo0 スピードは前方と右方の2つ必要
	const float speedForward = 0.1f;
	const float speedRight = 0.1f;

	const float rotForward = 0.5f;
	const float rotRight = 0.5f;

	//前方ベクトルと右方向ベクトル求める
	XMVECTOR forwardVec = calculation->ForwardVector(speedForward, enemyPos, position);
	XMVECTOR rotForwardVec = calculation->ForwardVector(rotForward, enemyPos, position);
	XMVECTOR rightVec = calculation->RightVector(speedRight, enemyPos, position);
	XMVECTOR rotRightVec = calculation->RightVector(rotRight, enemyPos, position);

	isWalk = false;
	//キーを押した時の処理
	if (state_ == MOVE || state_ == WAIT)
	{
		if (Input::GetInstance()->PushKey(DIK_D))
		{
			SetMove(rightVec, rotRightVec);
			direction = Right;
		}
		else if (Input::GetInstance()->PushKey(DIK_A))
		{
			SetMove(-rightVec, -rotRightVec);
			direction = Left;
		}

		if (Input::GetInstance()->PushKey(DIK_W))
		{
			SetMove(forwardVec, rotForwardVec);
			direction = Previous;
		}
		else if (Input::GetInstance()->PushKey(DIK_S))
		{
			SetMove(-forwardVec, -rotForwardVec);
			direction = Back;
		}

		//うごいている間fbxのモデル自体を回転
		if (state_ == MOVE)
		{
			fbxPlayer->SetIsRot(true);
			fbxPlayer->SetMatRot(inverseRot);
		}

		//何もキーに触れていなかったらfbxのアニメーションを待機に変える
		if (!Input::GetInstance()->PushKey(DIK_S) && !Input::GetInstance()->PushKey(DIK_W) && !Input::GetInstance()->PushKey(DIK_D) && !Input::GetInstance()->PushKey(DIK_A))
		{
			state_ = WAIT;
		}
	}
	
	//逆回転行列求める
	inverseRot = calculation->InverseRot(position, rotPos);
	
	fbxPlayer->SetPosition(position);
}

void Player::Dodge(XMFLOAT3 enemyPos)
{
	if (Input::GetInstance()->PushKey(DIK_K))
	{
		state_ = DODGE;
		fbxPlayer->SetNowTime(fbxPlayer->GetStartTime());
	}

	if (state_ == DODGE)
	{
		oldPos = position;
		///Todo0 スピードは前方と右方の2つ必要
		const float speedForward = 0.5f;
		const float speedRight = 0.5f;
		//前方ベクトルと右方向ベクトル求める
		XMVECTOR forwardVec = calculation->ForwardVector(speedForward, enemyPos, position);
		XMVECTOR rightVec = calculation->RightVector(speedRight, enemyPos, position);
		//移動後の座標を計算
		XMFLOAT3 endPos = {};
		if (direction == Previous)
		{
			endPos = calculation->AddVec(position, forwardVec);
		}
		else if (direction == Back)
		{
			endPos = calculation->AddVec(position, -forwardVec);
		}
		else if (direction == Right)
		{
			endPos = calculation->AddVec(position, rightVec);
		}
		else if (direction == Left)
		{
			endPos = calculation->AddVec(position, -rightVec);
		}
		//イージングで移動
		const float frame = 0.05f;
		dodgetime += frame;
		position = easing->ease(oldPos, endPos, dodgetime);
		if (dodgetime >= easing->maxflame)
		{
			dodgetime = 0;
			state_ = WAIT;
		}

		//fbxの処理
		//ダメージ時のアニメーション処理
		SetMoveFbxTime((fbxPlayer->GetFrame() * 2), fbxPlayer->GetEndTime());
	}
}

void Player::knockBack(DirectXCommon* dxCommon, Camera* camera, XMFLOAT3 enemyPos)
{
	const float speedForward = 0.5f;
	///プレイヤーから敵へのベクトルを求める
	XMVECTOR forwardVec = calculation->ForwardVector(speedForward, enemyPos, position);
	//前フレームの座標と移動後の座標
	knock_OldPos = position;
	knock_EndPos = calculation->AddVec(knock_OldPos, -forwardVec);
	//前フレームの座標から移動後の座標へ移動する処理
	if (isKnock && HP > 0)
	{
		//fbxの処理
		state_ = DAMAGE;
		fbxPlayer->SetNowTime(fbxPlayer->GetStartTime());
		//移動後の座標までの移動処理
		knockTime += 0.1f;
		position = easing->ease(knock_OldPos, knock_EndPos, knockTime);
		isEffect = true;

		if (knockTime >= easing->maxflame)
		{
			knockTime = 0;
			isKnock = false;
		}
	}

	//ダメージのアニメーション処理
	if (state_ == DAMAGE)
	{
		//ダメージ時のアニメーション処理
		SetMoveFbxTime((fbxPlayer->GetFrame() * 2), fbxPlayer->GetEndTime() - (fbxPlayer->GetFrame() * 3));
	}
	//ダメージえんしゅつ
	if (isEffect)
	{
		effectTime += 0.1f;
		effects->Load(L"effectsTest/10/fire_1.efk");
		effects->Play();
		effects->SetPosition({ position.x, position.y + 1.0f, position.z });
		effects->SetScale({ 0.1f,0.1f,0.1f });
		effects->SetSpeed(5);
		effects->Update(dxCommon->GetCmdList(), camera);
		if (effectTime > effectMaxTime)
		{
			effects->Stop();
			isEffect = false;
			effectTime = 0;
		}
	}

	fbxPlayer->SetPosition(position);
}

void Player::Attack(XMFLOAT3 enemyPos)
{
	const int attackTime = 70;
	const int combo1Time = 120;
	const int combo2Time = 170;

	if (state_ == WAIT || state_ == MOVE)
	{
		if (Input::GetInstance()->TriggerKey(DIK_L))
		{
			state_ = ATTACK;
			fbxPlayer->SetNowTime(fbxPlayer->GetStartTime());
			direction = Previous;
		}
	}
	else if (state_ == ATTACK)
	{
		Combo(2, attackTime, ATTACK_2, enemyPos);
	}
	else if (state_ == ATTACK_2)
	{
		Combo((attackTime + 2), combo1Time, ATTACK_3, enemyPos);
	}
	else if(state_ == ATTACK_3)
	{
		SetInverseRot(enemyPos);
		SetMoveFbxTime(fbxPlayer->GetFrame(), fbxPlayer->GetFrame() * combo2Time);
		if (fbxPlayer->GetNowTime() >= fbxPlayer->GetFrame() * combo2Time)
		{
			state_ = WAIT;
			fbxPlayer->SetNowTime(fbxPlayer->GetStartTime());
		}
	}
}

void Player::Combo(int min, int max, STATE type, XMFLOAT3 enemyPos)
{
	if (fbxPlayer->GetNowTime() >= fbxPlayer->GetFrame() * min && fbxPlayer->GetNowTime() < fbxPlayer->GetFrame() * (max - 1))
	{
		if (Input::GetInstance()->TriggerKey(DIK_L))
		{
			isCombo = true;
		}
	}
	if (fbxPlayer->GetNowTime() >= fbxPlayer->GetFrame() * max && isCombo)
	{
		state_ = type;
		fbxPlayer->SetNowTime(fbxPlayer->GetFrame() * (max + 1));
		direction = Previous;
		isCombo = false;
	}
	else if (fbxPlayer->GetNowTime() >= fbxPlayer->GetFrame() * max && !isCombo)
	{
		SetEndAnimeInit(&AnimationTime);
	}

	SetInverseRot(enemyPos);
	//攻撃時のアニメーション処理
	AnimationTime = fbxPlayer->GetNowTime() + fbxPlayer->GetFrame();
	fbxPlayer->SetNowTime(AnimationTime);
}

void Player::Death()
{
	if (HP <= 0)
	{
		state_ = DEATH;
	}

	if (state_ == DEATH)
	{
		//死亡時のアニメーション処理
		if (fbxPlayer->GetNowTime() >= fbxPlayer->GetEndTime())
		{
			isDeath = true;
			SetEndAnimeInit();
		}
	}
}

void Player::SetHpSprite()
{
	//HPバーのサイズ
	float sizeX = 679.0f;
	float hpSize = (sizeX / maxHP) * HP;
	HP = max(HP, 0);
	hpSize = max(hpSize, 0);

	HPBar->SetSize({ hpSize, 22.0f });
}

void Player::SetMoveFbxTime(FbxTime frame, FbxTime maxTime)
{
	//攻撃時のアニメーション処理
	AnimationTime = fbxPlayer->GetNowTime() + frame;
	fbxPlayer->SetNowTime(AnimationTime);
	if (fbxPlayer->GetNowTime() >= maxTime)
	{
		SetEndAnimeInit(&AnimationTime);
	}
}

void Player::SetEndAnimeInit(FbxTime* time)
{
	//攻撃などのアニメーションが終わった後の処理
	state_ = WAIT;
	fbxPlayer->SetNowTime(fbxPlayer->GetStartTime());

	if (time)
	{
		time = &fbxPlayer->GetStartTime();
	}
}

void Player::SetInverseRot(XMFLOAT3 enemyPos)
{
	//回転行列を逆行列に変換
	inverseRot = calculation->InverseRot(position, enemyPos);
	//逆回転をfbxの回転行列にセット
	fbxPlayer->SetIsRot(true);
	fbxPlayer->SetMatRot(inverseRot);
}

void Player::Draw(DirectXCommon* dxCommon)
{
	fbxPlayer->Draw(dxCommon->GetCmdList());
	effects->Draw(dxCommon->GetCmdList());
}

void Player::SpriteDraw()
{
	HPBar->Draw();
}


