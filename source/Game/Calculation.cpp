#include "Calculation.h"
#include <random>
using namespace DirectX;

XMFLOAT3 Calculation::AddVec(const XMFLOAT3& pos, const XMVECTOR& vec)
{
	XMFLOAT3 add = {};
	add.x = pos.x + vec.m128_f32[0];
	add.y = pos.y + vec.m128_f32[1];
	add.z = pos.z + vec.m128_f32[2];

	return add;
}

XMFLOAT3 Calculation::GetBonePosition(std::string boneName, FbxObject3d* fbxObj)
{
	// ボーンの計算済みワールド行列（ボーンのローカル行列×プレイヤーのワールド行列）
	XMMATRIX boneWorldMatrix = fbxObj->GetMatNowPose(fbxObj->GetBoneName(boneName)) * fbxObj->GetMatWorld();

	XMFLOAT3 pos = {
		boneWorldMatrix.r[3].m128_f32[0],
		boneWorldMatrix.r[3].m128_f32[1],
		boneWorldMatrix.r[3].m128_f32[2]
	};
	return pos;
}

XMVECTOR Calculation::ForwardVector(float speed, XMFLOAT3 enemyPos, XMFLOAT3 playerPos)
{
	///Todo0 スピードは前方と右方の2つ必要
	const float speedForward = speed;

	///Todo1 プレイヤーから敵へのベクトルを求める
	XMVECTOR forwardVec = {};
	forwardVec.m128_f32[0] = enemyPos.x - playerPos.x;
	forwardVec.m128_f32[1] = 0;
	forwardVec.m128_f32[2] = enemyPos.z - playerPos.z;

	///Todo2 正規化する（前方ベクトル/進行方向ベクトル）
	forwardVec = XMVector3Normalize(forwardVec);

	///Todo4 前方ベクトルに前向きスピードをかけて前方向の速度ベクトルを求める
	forwardVec *= speedForward;
	return forwardVec;
}

XMVECTOR Calculation::RightVector(float speed, XMFLOAT3 enemyPos, XMFLOAT3 playerPos)
{
	///Todo0 スピードは前方と右方の2つ必要
	const float speedRight = speed;

	///Todo1 プレイヤーから敵へのベクトルを求める
	XMVECTOR forwardVec = {};
	forwardVec.m128_f32[0] = enemyPos.x - playerPos.x;
	forwardVec.m128_f32[1] = 0;
	forwardVec.m128_f32[2] = enemyPos.z - playerPos.z;

	///Todo2 正規化する（前方ベクトル/進行方向ベクトル）
	forwardVec = XMVector3Normalize(forwardVec);

	///Todo3 外積によって右方向ベクトル（進行方向から見て右方向）を求める
	XMVECTOR worldUp = { 0,1,0,0 };
	XMVECTOR rightVec = XMVector3Cross(worldUp, forwardVec);

	///Todo5 右方向ベクトルに右向きスピードをかけて右方向の速度ベクトルを求める
	rightVec *= speedRight;
	return rightVec;
}

XMFLOAT3 Calculation::RotationYVelocity(XMVECTOR distance, float angle)
{
	XMVECTOR v0;
	v0 = distance;
	//angleラジアンだけy軸まわりに回転。半径は-100
	XMMATRIX rotM;
	rotM = XMMatrixIdentity();
	rotM = XMMatrixRotationY(XMConvertToRadians(angle));
	XMVECTOR v;
	v = XMVector3TransformNormal(v0, rotM);

	//回転後のスピード
	XMFLOAT3 velocity;
	velocity = { v.m128_f32[0],0.05f,v.m128_f32[2] };
	return velocity;
}

XMFLOAT3 Calculation::RotationXVelocity(XMVECTOR distance, float angle)
{
	XMVECTOR v0;
	v0 = distance;
	//angleラジアンだけy軸まわりに回転。半径は-100
	XMMATRIX rotM;
	rotM = XMMatrixIdentity();
	rotM = XMMatrixRotationX(XMConvertToRadians(angle));
	XMVECTOR v;
	v = XMVector3TransformNormal(v0, rotM);

	//回転後のスピード
	XMFLOAT3 velocity;
	velocity = { v.m128_f32[0],0.05f,v.m128_f32[2] };
	return velocity;
}

XMFLOAT3 Calculation::RotationZVelocity(XMVECTOR distance, float angle)
{
	XMVECTOR v0;
	v0 = distance;
	//angleラジアンだけy軸まわりに回転。半径は-100
	XMMATRIX rotM;
	rotM = XMMatrixIdentity();
	rotM = XMMatrixRotationZ(XMConvertToRadians(angle));
	XMVECTOR v;
	v = XMVector3TransformNormal(v0, rotM);

	//回転後のスピード
	XMFLOAT3 velocity;
	velocity = { v.m128_f32[0],v.m128_f32[1],v.m128_f32[2]};
	return velocity;
}

float Calculation::Difference(XMFLOAT3 a, XMFLOAT3 b)
{
	float dx = a.x - b.x;
	float dz = a.z - b.z;
	float difference = (float)sqrt(dx * dx + dz * dz);
	return difference;
}

XMMATRIX Calculation::InverseRot(XMFLOAT3 pos1, XMFLOAT3 pos2)
{
	//プレイヤーの回転
	XMVECTOR worldUp = { 0,1,0,0 };
	XMMATRIX rot;
	rot = XMMatrixLookAtLH(XMLoadFloat3(&pos1), XMLoadFloat3(&pos2), worldUp);
	//回転行列を逆行列に変換
	XMMATRIX inverseRot = XMMatrixInverse(NULL, rot);
	//回転行列の座標部分を0にし、回転だけ残す
	inverseRot.r[3].m128_f32[0] = 0;
	inverseRot.r[3].m128_f32[1] = 0;
	inverseRot.r[3].m128_f32[2] = 0;
	return inverseRot;
}

uint64_t Calculation::Get_rand_range(uint64_t min_val, uint64_t max_val)
{
	static std::mt19937_64 mt64(0);

	std::uniform_int_distribution<uint64_t> get_rand_uni_int(min_val, max_val);

	return get_rand_uni_int(mt64);
}
