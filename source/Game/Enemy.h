#pragma once
#include"DirectXCommon.h"
#include"Camera.h"
#include"Object3d.h"
#include"Object3dModel.h"
#include"Player.h"
#include"easing.h"
#include"Collision.h"
#include"CollisionPrimitive.h"
#include"Effects.h"
#include"fbxsdk.h"
#include"FbxLoader.h"
#include"FbxObject3d.h"
#include"Sprite.h"
#include"EnemyBullet.h"
#include"memory"
#include<list>
#include"Calculation.h"

class Enemy
{
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;

private:
	//ホーミング弾
	struct Point
	{
		XMFLOAT3 p0 = { 0,0,0 };
		XMFLOAT3 p1 = { 0,0,0 };
		XMFLOAT3 p2 = { 0,0,0 };
		XMFLOAT3 p3 = { 0,0,0 };
	};

public:
	Enemy();
	~Enemy();
	//一度だけの初期化
	void Initialize(DirectXCommon* dxCommon, Camera* camera);
	//ゲーム内初期化
	void Init();
	//更新
	void Update(Player* player, DirectXCommon* dxCommon, Camera* camera);
	//リソースの更新
	void ResourceUpdate();
	//行動切り替えの処理
	void Move(Player* player);
	//playerがいる方向に向く
	void Target(Player* player);
	//突進
	void Assault(Player* player);
	//ホーミング処理
	void HomingBullet(Player* player);
	//バウンドする弾の処理
	void BoundBullet(Player* player);
	//球弾のangle
	float BallAngle(XMVECTOR bDis, XMVECTOR cDis, const int maxBall, int num);
	//球弾の処理
	void BallBullet();
	//回転する弾の処理
	void CircleBalletInit(EnemyBullet* bullet, float angle, int count);
	void EnemyCircle();
	//直線に出る弾
	void RLMove(XMVECTOR lim, float initAngle);
	void StraightBullet();
	//ホーミング弾_2
	void Homing_2_Init(Player *player, Point point);
	void Homing_2_Bullet(Player* player);
	//弾のカウント
	int BulletCount(int count);
	//ダメージが入った時のFBXの処理
	void Damage(Player* player);
	//攻撃時の処理
	void Attack(Player* player);
	//死んだときのFBXなどの処理
	void Death();
	//プレイヤーとエネミーの当たり判定
	void PlayerenemyCollision(Player* player);
	//プレイヤーと弾の当たり判定
	void BulletCollision(Player* player, EnemyBullet* bullet);
	void CheckAllCollision(Player* player);
	//HPバーのサイズ変更
	void SetHpSprite();
	//FBXのアニメーションを止める
	void FbxStop();
	//アニーションの時間をセット
	void SetFbxTime();

	void Draw(DirectXCommon* dxCommon);

	void PostEffectDraw();

	void DrawSprite();

	const XMFLOAT3& GetPosition() { return position; }

	int SetHP(int hp) { return HP = hp; }
	int GetHP() { return HP; }
	float GetDifference() { return difference; }
	float GetAngle() { return Angle; }
	void SetIsDecrease(bool isDecrease) { this->isDecrease = isDecrease; }
	XMMATRIX GetCollisionMAt() { return spineMat; }
	void SetIsColl(bool isColl) { this->isColl = isColl; }
	void SetPosition(XMFLOAT3 position) { this->position = position; }
	float GetStAngle() { return stAngle; }

	//突進前の時間
	bool GetAssaltFlag() { return assaultFlag; }
	//死フラグ
	bool GetIsDeath() { return isDeath; }
	//プレイヤーステート取得
	int GetState() { return state_; }

protected:
	static const int maxBullet = 10;
	static const int BULLET_MAX = 50;
	static const int CIRCLE_MAX = 3;

	enum class Phase
	{
		ATTACK, //プレイヤーが敵に近かったら攻撃
		ASSALT, //突進
		HOMIG, //ホーミング弾
		CIRCLE, //サークル弾
		WALK,	//何もしない時間
		Bound,	//バウンド
		BALL,	//球
		STRAIGHT, //直進の弾
		HOMING_2 //ホーミングその2
	};

public:
	std::unique_ptr<FbxModel> enemyModel = nullptr;
	FbxObject3d* fbxEnemy = nullptr;
	//2d
	Sprite* HPBar;

	Easing* easing = nullptr;
	Effects* effects = nullptr;
	Calculation* calculation;

	float speed = 0.5f;

	float Angle;

	// ローカルスケール
	XMFLOAT3 scale = { 1,1,1 };
	// X,Y,Z軸回りのローカル回転角
	XMFLOAT3 rotation = { 0,0,0 };
	// ローカル座標
	XMFLOAT3 position = { 0,0,-5.0f };
	//前フレームの座標
	XMFLOAT3 playerOldPos = { 0,0,0 };
	XMFLOAT3 oldPos = { 0,0,0 };
	XMFLOAT3 pOldPos = { 0,0,0 };


	enum STATE
	{
		ASSALT,	//突進
		DAMAGE,	//ダメージ
		ATTACK,	//攻撃
		DEATH,	//死
		JAMP,	//ジャンプ
		WALK	//歩き
	};

	//
	const float angle_halfCircle = 180.0f;

	//プレイヤーに向く処理
	float targetTime = 0;
	const float maxTargetTime = 10;
	bool targetFlag = false;

	//突撃
	float preAssaultTime = 0;
	const float maxPreAssaultTime = 30;
	float bfoAssaultTime = 0;
	const float maxBfoAssaultTime = 80;
	bool assaultFlag = false;
	
	FbxTime animeTime = 0;

	//バウンド
	float gravity = 9.8f / 60.0f;
	float boundTime = 0;
	bool isBound = false;
	FbxTime fbxBoundTime = 0;
	bool boundFlag = false;
	float boundAngle = 30.0f;
	const float PI = 3.141592f;
	bool isBoundShot = false;
	float boundShotTime = 1.0f;
	const float maxBoundTime = 0.7f;
	bool isColl = false;
	int boundCount = 0;
	float boundeaseTime = 0;
	//ホーミング
	Point point;
	bool homingBulletFlag[BULLET_MAX];
	bool homingIsShot;
	float homingTime;
	const float maxHomingTime = 2.0f;
	uint64_t randX;
	bool isHoming = false;
	//寿命<frm>
	static const int32_t hEndTime = 60 * 3;
	//デスタイマー
	int32_t hStartTimer_ = hEndTime;
	int bulletCount = 0;
	//アニメーションストップ
	bool HStop = false;
	//寿命<frm>
	static const int32_t maxStopTime = 60 * 1;
	//デスタイマー
	int32_t StopTimer_ = maxStopTime;

	//プレイヤーと敵の当たり判定
	Sphere enemySphere[2];
	Sphere playerSphere;
	Sphere enemyBulletSp;
	bool isHit_Right;
	bool isHit_Left;
	float enemyRad = 0.6f;
	float playerRad = 0.3f;
	float bulletRad = 0.25f;
	bool Hit = false;

	int HP;
	const int maxHP = 10;

	//プレイヤーとエネミーの距離
	float difference = 0;

	//damage変数
	FbxTime damageTime = 0;
	bool isDecrease = false;

	//Attack変数
	bool playAttack = false;
	float attackEaseTime = 0;

	//Move変数
	float moveTime = 0;
	float maxMoveTime = 0;
	bool isMove = false;
	bool isDifference = false;
	float halfCount = 0;
	//ホーミングか球か
	uint64_t HBRand = 0;
	XMMATRIX spineMat;
	XMMATRIX spineTrans;
	XMMATRIX spineWorld;

	//弾
	float bulletAngle = 0;
	float circleTime = 1.0f;
	const float maxCircleTime = 3.0f;
	bool isCircle = false;
	float circleAngle = 0;
	int circleCount = 0;

	//球
	float fAngle = 0;
	//フェーズ
	Phase phase_ = Phase::WALK;
	//最大
	static const int COUNTER_MAX = 4;
	//タイマー
	float ballTime = 0;
	int bulletNum = 0;
	int num = 0;
	int count = 0;
	//フラグ
	bool isBall = false;
	bool BStop = false;
	XMVECTOR ballDis;
	XMFLOAT3 proPos = {0,0,0};
	float proBallAngle = 0;
	XMVECTOR cDis = {};

	//ゲームオーバーfbx変数
	bool isDeath = false;

	float time;

	//直線に進む弾幕
	static const int STRAIGHT_MAX = 9;
	float stTime = 0;
	int stCount = 0;
	bool isChange = false;
	float stAngle = 0;
	bool isStStpot = false;
	bool isSt = false;
	//寿命<frm>
	static const int32_t stEndTime = 60 * 5;
	//デスタイマー
	int32_t stStartTimer_ = stEndTime;
	//
	int hp2Count = 0;

	//ホーミング弾_2
	static const int HOMING_MAX = 6;
	Point point_2[HOMING_MAX];
	bool isHoming_2 = false;
	bool H2Stop = false;
	float homingTime_2;
	//デスタイマー
	int32_t h2StartTimer_ = stEndTime;
	
	//弾初期化
	EnemyBullet *homingBullet;
	EnemyBullet *boundBullet;
	EnemyBullet* CircleBullet[CIRCLE_MAX];
	EnemyBullet* flowerBullet[COUNTER_MAX];
	EnemyBullet* straightBullet[STRAIGHT_MAX];
	EnemyBullet* homingBullet_2[HOMING_MAX];

	//行動fbx切り替え
	STATE state_;
};
