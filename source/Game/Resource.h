#pragma once

#include <DirectXMath.h>

#include<d3dx12.h>
#include<xaudio2.h>
#include<fstream>
#include"Input.h"
#include"WinApp.h"
#include"DirectXCommon.h"
#include"Object3d.h"
#include"Object3dModel.h"
#include"Sprite.h"
#include"DebugText.h" 
#include"Collision.h"
#include"CollisionPrimitive.h"
#include"fbxsdk.h"
#include"FbxLoader.h"
#include"FbxObject3d.h"


using namespace Microsoft::WRL;
using namespace DirectX;

class Resource
{
private:
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;
public:
	Resource();
	~Resource();

	void Initialize(DirectXCommon* dxc);
	void Update();
	void EffectPreDraw();
	void EffectBackDraw();
	void Draw();

private:
	DirectXCommon* dxCommon = nullptr;

private://3Dモデル
	Object3dModel* posterModel = nullptr;
	Object3d* poster = nullptr;
	Object3dModel* posterModel_2 = nullptr;
	Object3d* poster_2 = nullptr;
	Object3dModel* boardModel = nullptr;
	Object3d* objBoard = nullptr;
	Object3dModel* boardModel_2 = nullptr;
	Object3d* objBoard_2 = nullptr;
	Object3dModel* wallModel = nullptr;
	Object3d* objWall = nullptr;
	Object3dModel* signboardModel = nullptr;
	Object3d* objSignboard = nullptr;
	Object3dModel* arcardModel = nullptr;
	Object3d* objArcard = nullptr;
	Object3dModel* arcardModel_2 = nullptr;
	Object3d* objArcard_2 = nullptr;
	Object3dModel* boxModel = nullptr;
	Object3d* objBox = nullptr;
	Object3dModel* sofaModel = nullptr;
	Object3d* objSofa = nullptr;
	Object3dModel* sofa_2Model = nullptr;
	Object3d* objSofa_2 = nullptr;
	Object3dModel* tableModel = nullptr;
	Object3d* objTable = nullptr;
	Object3dModel* floorModel = nullptr;
	Object3d* objFloor = nullptr;
	Object3dModel* spModel = nullptr;
	Object3d* objRedArcard_1 = nullptr;
	Object3dModel* redArcard_1Model = nullptr;
	Object3d* objRedArcard_2 = nullptr;
	Object3dModel* redArcard_2Model = nullptr;
	Object3d* objRedArcard_3 = nullptr;
	Object3dModel* redArcard_3Model = nullptr;
	Object3d* objRedArcard2_1 = nullptr;
	Object3d* objRedArcard2_2 = nullptr;
	Object3d* objRedArcard2_3 = nullptr;
	Object3dModel* redArcard_gameModel = nullptr;
	Object3d* objRedArcard_game = nullptr;
	Object3dModel* redArcard2_gameModel = nullptr;
	Object3d* objRedArcard2_game = nullptr;
	Object3dModel* arcard_gameModel = nullptr;
	Object3d* objArcard_game = nullptr;
	Object3dModel* arcard2_gameModel = nullptr;
	Object3d* objArcard2_game = nullptr;

private://FBXモデル


private://2D


public:
	
};
