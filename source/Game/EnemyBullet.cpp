#include "EnemyBullet.h"

EnemyBullet::EnemyBullet()
{
}

EnemyBullet::~EnemyBullet()
{
}

void EnemyBullet::Initialize()
{
	enemyBulletModel = Object3dModel::LoadFromOBJ("bullet");
	objEnemyBullet = Object3d::Create();
	objEnemyBullet->InitializeGraphicsPipeline(L"Resource/shaders/instVS.hlsl", L"Resource/shaders/OBJPixelShader.hlsl");
	objEnemyBullet->SetObject3dModel(enemyBulletModel);
	for (int i = 0; i < BULLET_MAX; i++)
	{
		objEnemyBullet->SetInstScale({ 0.25,0.25,0.25 }, i);
		isAlive[i] = true;
		deathTimer_[i] = KLifeTime;
		objEnemyBullet->SetInstPosition({ 2,0,2 }, i);
	}
}

void EnemyBullet::Init(XMFLOAT3 position, XMFLOAT3 endPos, XMFLOAT3 speed, int num)
{
	enemyBulletPosition[num] = {position.x,  position.y, position.z};
	this->endPos[num] = endPos;
	bulletSpeed = speed;
	objEnemyBullet->SetInstPosition(enemyBulletPosition[num], num);
	angle = 0;
	boundTime[num] = 0;
	deathTimer_[num] = KLifeTime;
}

void EnemyBullet::StraightBullet(int num)
{
	if (isAlive[num] == false)
	{
		enemyBulletPosition[num].x += bulletSpeed.x;
		enemyBulletPosition[num].z += bulletSpeed.z;
		boundTime[num]++;

		if (--deathTimer_[num] <= 0)
		{
			isAlive[num] = true;
		}
	}
	if (isAlive[num] == true)
	{
		enemyBulletPosition[num] = { 2,0,2 };
	}

	objEnemyBullet->SetInstRotation({ boundTime[num] * 10, 0,0 }, num);
	objEnemyBullet->SetInstPosition(enemyBulletPosition[num], num);
}

void EnemyBullet::CircleBullet(int num)
{
	if (isAlive[num] == false)
	{
		enemyBulletPosition[num].x += bulletSpeed.x;
		enemyBulletPosition[num].y -= bulletSpeed.y;
		enemyBulletPosition[num].z += bulletSpeed.z;
		boundTime[num]++;

		if (enemyBulletPosition[num].y <= 0.5f)
		{
			enemyBulletPosition[num].y = 0.5f;
		}

		if (--deathTimer_[num] <= 0)
		{
			isAlive[num] = true;
		}
	}
	if (isAlive[num] == true)
	{
		enemyBulletPosition[num] = { 2,0,2 };
	}
	
	objEnemyBullet->SetInstRotation({ boundTime[num] * 10, 0,0 }, num);
	objEnemyBullet->SetInstPosition(enemyBulletPosition[num], num);
}

void EnemyBullet::BoundBullet(Easing *easing, int num)
{
	if (isAlive[num] == false)
	{
		XMFLOAT3 pos[BULLET_MAX];
		pos[num] = {enemyBulletPosition[num].x, enemyBulletPosition[num].y + 0.5f, enemyBulletPosition[num].z + 1.0f};
		enemyBulletPosition[num] = easing->easeOut_Bounce(pos[num], endPos[num], boundTime[num]);
		boundTime[num] += 0.01f;

		if (boundTime[num] >= easing->maxflame)
		{
			isAlive[num] = true;
			boundTime[num] = easing->maxflame;
		}
	}
	for (int i = 0; i < BULLET_MAX; i++)
	{
		if (isAlive[i] == true)
		{
			enemyBulletPosition[i] = { 5,0,5 };
		}

		objEnemyBullet->SetInstPosition(enemyBulletPosition[i], i);
	}
	

	objEnemyBullet->SetInstRotation({ boundTime[num] * 1000, 0,0 }, num);
}

void EnemyBullet::HomingInit(XMFLOAT3 position, XMFLOAT3 point1, XMFLOAT3 point2, XMFLOAT3 point3, XMFLOAT3 speed, int num)
{
	enemyBulletPosition[num] = position;
	point[num].p0 = enemyBulletPosition[num];
	point[num].p1 = point1;
	point[num].p2 = point2;
	point[num].p3 = point3;
	homingTime[num] = 0;
	deathTimer_[num] = KLifeTime;
	bulletSpeed = speed;
}

void EnemyBullet::Homing(Easing *easing, int num)
{
	if (isAlive[num] == false)
	{
		homingTime[num] += bulletSpeed.x;
		a[num] = easing->lerp(point[num].p0, point[num].p1, homingTime[num]);
		b[num] = easing->lerp(point[num].p1, point[num].p2, homingTime[num]);
		c[num] = easing->lerp(point[num].p2, point[num].p3, homingTime[num]);
		d[num] = easing->lerp(a[num], b[num], homingTime[num]);
		e[num] = easing->lerp(b[num], c[num], homingTime[num]);
		enemyBulletPosition[num] = easing->lerp(d[num], e[num], homingTime[num]);

		if (--deathTimer_[num] <= 0)
		{
			isAlive[num] = true;
		}
	}
	for (int i = 0; i < BULLET_MAX; i++)
	{
		if (isAlive[i] == true)
		{
			enemyBulletPosition[i] = { 5,0,5 };
			homingTime[i] = 0;
		}

		objEnemyBullet->SetInstPosition(enemyBulletPosition[i], i);
	}

	objEnemyBullet->SetInstRotation({ homingTime[num] * 1000, 0,0 }, num);
}

void EnemyBullet::FlowerInit(XMFLOAT3 position, XMFLOAT3 speed, XMVECTOR distance, XMFLOAT3 proPos, float angle, int num)
{
	enemyBulletPosition[num] = position;
	this->proPos[num] = proPos;
	bulletSpeed = speed;
	deathTimer_[num] = KLifeTime;
	boundTime[num] = 0;
	this->distance[num] = distance;
	ballAngle[num] = angle;
}

void EnemyBullet::FlowerBullet(XMFLOAT3 position)
{
	for (int i = 0; i < BULLET_MAX; i++)
	{
		if (isAlive[i] == false)
		{
			//仮座標の処理
			if (proPos[i].x > position.x + 0.2f)
			{
				proPos[i].x += bulletSpeed.x;
			}
			else if (proPos[i].x < position.x - 0.2f)
			{
				proPos[i].x -= bulletSpeed.x;
			}

			//弾の処理
			distance[i].m128_f32[1] += bulletSpeed.z;
			distance[i].m128_f32[2] += bulletSpeed.z;
			XMVECTOR v0;
			v0 = distance[i];
			//angleラジアンだけy軸まわりに回転。半径は-100
			XMMATRIX rotM;
			rotM = DirectX::XMMatrixIdentity();
			rotM = DirectX::XMMatrixRotationX(ballAngle[i]);
			XMVECTOR v;
			v = XMVector3TransformNormal(v0, rotM);
			XMVECTOR target = { proPos[i].x, proPos[i].y, proPos[i].z,0};
			XMVECTOR v3 = { target.m128_f32[0] + v.m128_f32[0], target.m128_f32[1] + v.m128_f32[1], target.m128_f32[2] + v.m128_f32[2] };
			XMFLOAT3 f = { v3.m128_f32[0], v3.m128_f32[1], v3.m128_f32[2] };

			enemyBulletPosition[i] = f;

			boundTime[i]++;

			if (--deathTimer_[i] <= 0)
			{
				isAlive[i] = true;
			}
		}
		if (isAlive[i] == true)
		{
			enemyBulletPosition[i] = { 2,0,2 };
		}

		objEnemyBullet->SetInstRotation({ boundTime[i] * 10, 0,0 }, i);
		objEnemyBullet->SetInstPosition(enemyBulletPosition[i], i);
	}
	
}

void EnemyBullet::Update()
{
	objEnemyBullet->Update();
}

void EnemyBullet::Draw()
{
	objEnemyBullet->InstDraw();
}

void EnemyBullet::OnCollision(int num)
{
	isAlive[num] = true;
	enemyBulletPosition[num] = { 2,0,2 };
	objEnemyBullet->SetInstPosition(enemyBulletPosition[num], num);
}
