#pragma once

#include <DirectXMath.h>

#include<d3dx12.h>
#include<xaudio2.h>
#include<fstream>
#include"Input.h"
#include"WinApp.h"
#include"DirectXCommon.h"
#include"Object3d.h"
#include"Object3dModel.h"
#include"Sprite.h"
#include"DebugText.h" 
#include"Collision.h"
#include"CollisionPrimitive.h"
#include"Audio.h"
#include"ParticleManager.h"
#include"DebugCamera.h"
#include"fbxsdk.h"
#include"FbxLoader.h"
#include"FbxObject3d.h"
#include"PostEffect.h"
#include"Light.h"
#include "LightGroup.h"
#include"Title.h"
#include"Calculation.h"
#include"Clear.h"
#include"GameScene.h"
#include"BaseScene.h"
#include"GameOver.h"


using namespace Microsoft::WRL;
using namespace DirectX;

class SceneManager
{
private:
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;
public:
	SceneManager();
	~SceneManager();

	void Initialize(DirectXCommon* dxc, Audio* sound);
	void Update();
	void Draw();

private:
	//シーン切り替え
	enum Scene
	{
		OP,
		PLAY,
		CLEAR,
		OVER
	};
private:
	std::unique_ptr<FbxModel> model_Run = nullptr;
	FbxObject3d* fbxRun = nullptr;
private:
	DirectXCommon* dxCommon = nullptr;
	ParticleManager* particleMan = nullptr;
	Audio* audio = nullptr;
	DebugText debugText;
	Camera* camera = nullptr;
	Light* light = nullptr;

	Title* opTitle = nullptr;
	GameScene* gameScene = nullptr;
	Clear* clear = nullptr;
	GameOver* gameover = nullptr;
	Calculation* calculation;
	//ライト
	LightGroup* lightGroup = nullptr;
	Scene scene_;
	int sceneNo = OP;

	BaseScene* m_pScene = nullptr;

};
