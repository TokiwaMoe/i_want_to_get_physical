#include "Clear.h"

Clear::Clear(){}
Clear::~Clear(){}

void Clear::Initialize(DirectXCommon* dxc, Audio* sound, Camera* camera, LightGroup* lightGroup, FbxObject3d* fbx)
{
	this->dxCommon = dxc;
	this->sound = sound;
	this->camera = camera;
	this->lightGroup = lightGroup;
	this->fbx = fbx;

	Sprite::LoadTexture(1, L"Resource/title.png");
	clear = Sprite::Create(1, { 0.0f,0.0f });
}

void Clear::Update()
{
	fbx->SetPosition({ 0,0,0 });
	fbx->PlayAnimation(fbx->GetArmature("pray"));
	fbx->SetArmatureNo(fbx->GetArmature("pray"));

	camera->TargetRot({ 0,5.0f,-9.0f }, { -3.0f,6.3f,0 }, { 220, 0, 0 });
	//ライト更新
	const int LIGHT_MAX = 3;
	lightGroup->SetDirLightActive(0, true);
	lightGroup->SetDirLightActive(1, false);
	lightGroup->SetDirLightActive(2, false);
	lightGroup->SetDirLightDir(0, XMVECTOR({ 0, -1, -2, 0 }));
	lightGroup->SetDirLightColor(0, { 1,1,1 });
	for (int i = 0; i < LIGHT_MAX; i++)
	{
		lightGroup->SetPointLightActive(i, false);
	}
	const int CIRCLE_MAX = 1;
	for (int i = 0; i < CIRCLE_MAX; i++)
	{
		lightGroup->SetCircleShadowActive(i, false);
		lightGroup->SetSpotLightActive(i, false);
	}
	
	fbx->Update();
}

void Clear::Object3dDraw()
{
	fbx->Draw(dxCommon->GetCmdList());
}

void Clear::Resource2dDraw()
{
	clear->Draw();
}
