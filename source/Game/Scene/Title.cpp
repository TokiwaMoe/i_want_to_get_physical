#include "Title.h"

Title::Title() {}

Title::~Title() {}

void Title::Initialize(DirectXCommon* dxc, Audio* sound, Camera* camera, LightGroup* lightGroup, FbxObject3d* fbx)
{
	this->dxCommon = dxc;
	this->sound = sound;
	this->camera = camera;
	this->lightGroup = lightGroup;
	this->fbx = fbx;
	boxModel = Object3dModel::LoadFromOBJ("box");

	objBox = Object3d::Create();
	objBox->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objBox->SetObject3dModel(boxModel);
	objBox->SetScale({ 1,1,1 });
	objBox->SetRotation({ 0,240,0 });

	easing = new Easing();
	easing->Initialize();

	Resource2dCreate();
}

void Title::Resource2dCreate()
{
	Sprite::LoadTexture(90, L"Resource/opSprite/frame.png");
	Sprite::LoadTexture(91, L"Resource/opSprite/op1.png");
	Sprite::LoadTexture(92, L"Resource/opSprite/op2.png");
	Sprite::LoadTexture(93, L"Resource/opSprite/op3.png");
	Sprite::LoadTexture(94, L"Resource/opSprite/op3_2.png");
	Sprite::LoadTexture(95, L"Resource/opSprite/op4.png");
	Sprite::LoadTexture(96, L"Resource/opSprite/op4_2.png");
	Sprite::LoadTexture(97, L"Resource/opSprite/i_want_to_get_physical.png");
	Sprite::LoadTexture(98, L"Resource/opSprite/logo.png");
	Sprite::LoadTexture(99, L"Resource/opSprite/both.png");
	Sprite::LoadTexture(100, L"Resource/opSprite/space.png");


	opFrame = Sprite::Create(90, { 0.0f,0.0f });
	op[god] = Sprite::Create(91, { 0.0f,0.0f });
	op[why] = Sprite::Create(92, { 0.0f,0.0f });
	op[2] = Sprite::Create(93, { -1280.0f,0.0f });
	op[3] = Sprite::Create(94, { 1280.0f,0.0f });
	op[4] = Sprite::Create(95, { -1280.0f,0.0f });
	op[5] = Sprite::Create(96, { 1280.0f,0.0f });
	op[6] = Sprite::Create(99, { 0.0f,0.0f });
	space = Sprite::Create(100, { 0.0f,0.0f });

	for (int i = 0; i < 22; i++)
	{
		logo = Sprite::Create(97, { 0,0 });
		logo->SetRotation(90);
		boneLogo = Sprite::Create(98, { 0,0 });
		boneLogo->SetRotation(90);
	}
}

void Title::GameInitialize()
{
	opNo = god;
	chengeTimer_1 = opChengeTime_;
	chengeTimer_2 = opChengeTime_2;
	chengeTimer_4 = opChengeTime_;
	chengeTimer_5 = opChengeTime_;
	chengeTimer_6 = opChengeTime_;
	chengeTimer_7 = opChengeTime_;
}

void Title::Set(Camera* camera, FbxObject3d* fbx, XMFLOAT3 position, XMVECTOR distance, XMFLOAT3 target, XMFLOAT3 angle, std::string name)
{
	fbx->SetPosition(position);
	camera->TargetRot(distance, target, angle);
	fbx->PlayAnimation(fbx->GetArmature(name));
	fbx->SetArmatureNo(fbx->GetArmature(name));
}

void Title::Update()
{
	switch (opNo)
	{
	case god:
		Set(camera, fbx, { 0,0,0 }, { 0,5,-10,0 }, { 0,6.3f,0 }, { 180,0,0 }, "pray");

		if (--chengeTimer_1 <= 0)
		{
			opNo = why;
		}
		break;
	case why:
		XMFLOAT3 angle;
		if (--chengeTimer_2 <= opChengeTime_2 / 2)
		{
			angle = { 270, 0, 0 };
		}
		else
		{
			angle = { 90, 0, 0 };
		}

		Set(camera, fbx, { 0,0,0 }, { 0,7.0f,-10.0f }, { 0,8.3f,0 }, angle, "run");

		if (--chengeTimer_2 <= 0)
		{
			opNo = trip;
			AnimationTime = fbx->GetStartTime();
			fbx->SetNowTime(fbx->GetStartTime());
		}
		break;
	case trip:
		AnimationTime = AnimationTime + (fbx->GetFrame() / 2);
		fbx->SetNowTime(AnimationTime);

		Set(camera, fbx, { 0,0,0 }, { 0,0,-20.0f }, calculation->GetBonePosition("Bip01_Spine2", fbx), { 90,0,0 }, "sTrip");

		if (AnimationTime >= fbx->GetEndTime())
		{
			opNo = revelation;
		}
		break;
	case revelation:
		XMFLOAT3 pos = calculation->GetBonePosition("Bip01_Spine2", fbx);
		Set(camera, fbx, { 0,0,0 }, { 3.0f,7.0f,-5.0f }, pos, { 270,0,0 }, "sTrip");
		//現在の時間セット
		fbx->SetNowTime(fbx->GetEndTime() - fbx->GetFrame());

		objBox->SetPosition({ pos.x - 10.0f, 0, pos.z - 1.0f });

		if (--chengeTimer_4 <= 0)
		{
			opNo = rule;
			ruleFrame = 0;
			op[2]->SetPosition({ -1280, 0 });
			op[3]->SetPosition({ 1280, 0 });
		}
		break;
	case rule:
		ruleFrame += frame;
		XMFLOAT3 rulePos_1 = easing->EaseSine_out({ -1280, 0, 0 }, { 0,0,0 }, ruleFrame);
		XMFLOAT3 rulePos_2 = easing->EaseSine_out({ 1280, 0, 0 }, { 0,0,0 }, ruleFrame);
		op[2]->SetPosition({ rulePos_1.x, rulePos_1.y });
		op[3]->SetPosition({ rulePos_2.x, rulePos_2.y });

		Set(camera, fbx, { -5.0f,0,0 }, { 0,5.0f,-15.0f }, { 0,6.3f,0 }, { 180, 0, 0 }, "attack");

		if (ruleFrame >= easing->maxflame)
		{
			op[2]->SetPosition({ 0,0 });
			op[3]->SetPosition({ 0,0 });
		}

		if (--chengeTimer_5 <= 0)
		{
			opNo = first_class;
			firstFrame = 0;
			op[4]->SetPosition({ -1280, 0 });
			op[5]->SetPosition({ 1280, 0 });
		}
		break;
	case first_class:
		firstFrame += frame;
		XMFLOAT3 rulePos_3 = easing->EaseSine_out({ -1280, 0, 0 }, { 0,0,0 }, firstFrame);
		XMFLOAT3 rulePos_4 = easing->EaseSine_out({ 1280, 0, 0 }, { 0,0,0 }, firstFrame);
		op[4]->SetPosition({ rulePos_3.x, rulePos_3.y });
		op[5]->SetPosition({ rulePos_4.x, rulePos_4.y });

		Set(camera, fbx, { 0,0,0 }, { 0,5.0f,-13.0f }, { 0,6.3f,0 }, { 180, 0, 0 }, "dance");

		if (firstFrame >= easing->maxflame)
		{
			op[4]->SetPosition({ 0,0 });
			op[5]->SetPosition({ 0,0 });
		}

		if (--chengeTimer_6 <= 0)
		{
			opNo = both;
		}
		break;
	case both:
		Set(camera, fbx, { 0,-2.0f,0 }, { 0,5.0f,-11.0f }, { 0,6.3f,0 }, { 180, 0, 0 }, "dance_2");

		if (--chengeTimer_7 <= 0)
		{
			chengeTimer_8 = opChengeTime_8;
			opNo = logoTitle;
			logoFrame = 0;
			logo->SetRotation(90);
			boneLogo->SetRotation(90);
		}
		break;
	case logoTitle:
		logoFrame += frame;
		XMFLOAT3 rotation = easing->EaseSine_out({ 90,0,0 }, { 0,0,0 }, logoFrame);
		logo->SetRotation(rotation.x);
		boneLogo->SetRotation(rotation.x);

		if (logoFrame >= easing->maxflame)
		{
			logo->SetRotation(0);
			boneLogo->SetRotation(0);
		}

		if (--chengeTimer_8 <= 0)
		{
			GameInitialize();
		}
		break;
	default:
		break;
	}
	{
		const int LIGHT_MAX = 3;
		lightGroup->SetCircleShadowActive(0, false);
		lightGroup->SetCircleShadowActive(1, false);
		lightGroup->SetDirLightActive(0, true);
		lightGroup->SetDirLightActive(1, false);
		lightGroup->SetDirLightActive(2, false);
		for (int i = 0; i < LIGHT_MAX; i++)
		{
			lightGroup->SetPointLightActive(i, false);
		}
		const int SPOTLIGHT_MAX = 7;
		for (int i = 0; i < SPOTLIGHT_MAX; i++)
		{
			lightGroup->SetSpotLightActive(i, false);
		}

		lightGroup->SetDirLightDir(0, XMVECTOR({ 0, -1, -1, 0 }));
		lightGroup->SetDirLightColor(0, { 1,1,1 });
	}

	objBox->Update();
	fbx->Update();
}

void Title::Object3dDraw()
{
	if (opNo != logoTitle)
	{
		fbx->Draw(dxCommon->GetCmdList());
	}

	if (opNo == revelation)
	{
		objBox->Draw();
	}
}

void Title::Resource2dDraw()
{
	if (opNo == god || opNo == why)
	{
		opFrame->Draw();
	}

	if (opNo == god)
	{
		op[god]->Draw();
	}
	else if (opNo == why)
	{
		op[why]->Draw();
	}
	else if (opNo == rule)
	{
		op[2]->Draw();
		op[3]->Draw();
	}
	else if (opNo == first_class)
	{
		op[4]->Draw();
		op[5]->Draw();
	}
	else if (opNo == both)
	{
		op[6]->Draw();
	}
	else if (opNo == logoTitle)
	{
		boneLogo->Draw();
		logo->Draw();
	}

	space->Draw();
}

