#include "SceneManager.h"

SceneManager::SceneManager(){}

SceneManager::~SceneManager(){}

void SceneManager::Initialize(DirectXCommon* dxc, Audio* sound)
{
	this->dxCommon = dxc;
	this->audio = sound;

#pragma region カメラ
	// カメラ生成
	camera = new Camera(WinApp::window_width, WinApp::window_height);
	// 3Dオブジェクトにカメラをセット
	Object3d::SetCamera(camera);
#pragma endregion

#pragma region ライト
	// ライト静的初期化
	LightGroup::StaticInitialize(dxCommon->GetDev());
	// ライト生成
	lightGroup = LightGroup::Create();
	// 3Dオブエクトにライトをセット
	Object3d::SetLightGroup(lightGroup);
	FbxObject3d::SetLightGroup(lightGroup);


	/*lightGroup->SetPointLightActive(0, true);
	lightGroup->SetSpotLightActive(1, true);*/
#pragma endregion

#pragma region Object3d
	Object3d::StaticInitialize(dxCommon->GetDev(), camera);

#pragma endregion

#pragma region FBX
	//デバイスをセット
	FbxObject3d::SetDevice(dxCommon->GetDev());
	//カメラをセット
	FbxObject3d::SetCamera(camera);
	//グラフィックパイプライン生成
	FbxObject3d::CreateGraphicsPipline();
	//FBX
	FbxLoader::GetInstance()->Initiallize(dxCommon->GetDev());
	model_Run = FbxLoader::GetInstance()->LoadMadelFromFile("op");
	fbxRun = new FbxObject3d;
	fbxRun->Initialize();
	fbxRun->SetModel(model_Run.get());
	fbxRun->LoadAnumation();
	fbxRun->SetScale({ 0.1f,0.1f,0.1f });
	fbxRun->SetRotation({ 0,0,0 });
	fbxRun->SetPosition({ 0,0,0 });

#pragma endregion

#pragma region スプライト
	Sprite::StaticInitialize(dxCommon->GetDev(), WinApp::window_width, WinApp::window_height);
#pragma endregion

#pragma region パーティクル
	particleMan = ParticleManager::Create(dxCommon->GetDev(), WinApp::window_width, WinApp::window_height);
#pragma endregion

#pragma region サウンド
	/*audio = new Audio;
	audio->Initialize();*/
#pragma endregion

	/*switch (sceneNo)
	{
	case OP:
		m_pScene = new Title();
		break;
	case PLAY:
		m_pScene = new GameScene();
		break;
	case CLEAR:
		m_pScene = new Clear();
		break;
	case OVER:
		m_pScene = new GameOver();
	default:
		break;
	}*/
	opTitle = new Title();
	opTitle->Initialize(dxCommon, audio, camera, lightGroup, fbxRun);

	clear = new Clear();
	clear->Initialize(dxCommon, audio, camera, lightGroup, fbxRun);

	gameScene = new GameScene();
	gameScene->Initialize(dxCommon, audio, camera, lightGroup, fbxRun);

	gameover = new GameOver();
	gameover->Initialize();
}

void SceneManager::Update()
{
	//DirectX毎フレーム処理　ここから
	if (sceneNo == OP)
	{
		opTitle->Update();

		if (Input::GetInstance()->TriggerKey(DIK_SPACE))
		{
			sceneNo = PLAY;
			gameScene->GameInitialize();
			camera->SetEye(gameScene->GetEnemyPos());
		}
	}

	else if (sceneNo == PLAY)
	{
		gameScene->Update();

		if (gameScene->EnemyGetIsDeath())
		{
			sceneNo = CLEAR;
			opTitle->GameInitialize();
		}

		if (gameScene->PlayerGetIsDeath())
		{
			sceneNo = OVER;
			opTitle->GameInitialize();
		}
	}

	else if (sceneNo == CLEAR)
	{
		clear->Update();

		if (Input::GetInstance()->TriggerKey(DIK_SPACE))
		{
			sceneNo = OP;
		}
	}

	if (sceneNo == OVER)
	{
		if (Input::GetInstance()->TriggerKey(DIK_SPACE))
		{
			sceneNo = OP;
		}
	}

	camera->Update();
	lightGroup->Update();
}

void SceneManager::Draw()
{
	gameScene->PostEffectDraw();

#pragma region 3D描画
	dxCommon->PreDraw();

	dxCommon->ClearDepthBuffer();

	Object3d::PreDraw(dxCommon->GetCmdList());
	if (sceneNo == OP)
	{
		opTitle->Object3dDraw();
	}

	if (sceneNo == PLAY)
	{
		gameScene->Object3dDraw();
	}

	if (sceneNo == CLEAR)
	{
		clear->Object3dDraw();
	}

	Object3d::PostDraw();

#pragma endregion

#pragma region 前景スプライト描画
	Sprite::PreDraw(dxCommon->GetCmdList());

	float strX = 30;

	if (sceneNo == OP)
	{
		opTitle->Resource2dDraw();
	}

	if (sceneNo == CLEAR)
	{
		clear->Resource2dDraw();
	}

	if (sceneNo == PLAY)
	{
		gameScene->Resource2dDraw();
	}

	if (sceneNo == OVER)
	{
		gameover->Draw();
	}

	debugText.DrawAll(dxCommon->GetCmdList());
	Sprite::PostDraw();

	dxCommon->PostDraw();
}
