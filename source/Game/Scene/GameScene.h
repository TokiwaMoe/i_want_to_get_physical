#pragma once

#include <DirectXMath.h>

#include<d3dx12.h>
#include<xaudio2.h>
#include<fstream>
#include"Input.h"
#include"WinApp.h"
#include"DirectXCommon.h"
#include"Object3d.h"
#include"Object3dModel.h"
#include"Sprite.h"
#include"DebugText.h" 
#include"Collision.h"
#include"CollisionPrimitive.h"
#include"Audio.h"
#include"ParticleManager.h"
#include"DebugCamera.h"
#include"fbxsdk.h"
#include"FbxLoader.h"
#include"FbxObject3d.h"
#include"PostEffect.h"
#include"Light.h"
#include"Player.h"
#include"Enemy.h"
#include"Sword.h"
#include"Effects.h"
#include"easing.h"
#include"debugObj.h"
#include "LightGroup.h"
#include"PostEffect.h"
#include"Resource.h"
#include"Title.h"
#include"Calculation.h"
#include"Clear.h"
#include"BaseScene.h"

using namespace Microsoft::WRL;
using namespace DirectX;

class GameScene
{
private:
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;
public:
	GameScene();
	~GameScene();

	void Initialize(DirectXCommon* dxc, Audio *sound, Camera* camera, LightGroup* lightGroup, FbxObject3d* fbx);
	void GameInitialize();
	void Update();
	void SetLightPos_Dir(XMFLOAT3 pos, XMVECTOR dir);
	void LightUpdate();
	void GameUpdate();
	void DamageCamera();
	void Object3dDraw();
	void Resource2dDraw();
	void PostEffectDraw();
	void Delete();
	void ObstacleCollision();
	void PlayerCollision(Sphere p1, Sphere p2, XMVECTOR reject_);
	void EnemyCollision(Sphere p1, Sphere p2, XMVECTOR reject_);
	void CameraCollision(Sphere p1, Sphere p2, XMVECTOR reject_);

	//getter
	bool EnemyGetIsDeath() { return enemy->GetIsDeath(); }
	bool PlayerGetIsDeath() { return player->GetIsDeath(); }
	XMFLOAT3 GetEnemyPos() { return enemy->GetPosition(); }

private:
	DirectXCommon* dxCommon = nullptr;
	ParticleManager* particleMan = nullptr;
	Audio* audio = nullptr;
	DebugText debugText;
	Camera* camera = nullptr;
	LightGroup* lightGroup = nullptr;
	static const int MAX_SMOKE = 4;
	Effects* effects[MAX_SMOKE];
	DebugObj* debugObj = nullptr;

	Player* player = nullptr;
	Enemy* enemy = nullptr;
	Sword* sword = nullptr;
	Calculation* calculation;


	int debugTextTexNumber = 0;

private://3Dモデル
	Object3dModel* boxModel = nullptr;
	Object3d* objBox = nullptr;

	Easing* easing = nullptr;
	Resource* resource = nullptr;

private://FBXモデル
	std::unique_ptr<FbxModel> model1 = nullptr;
	FbxObject3d* object1 = nullptr;

	std::unique_ptr<FbxModel> model_Run = nullptr;
	FbxObject3d* fbxRun = nullptr;

	FbxObject3d* fbx;

private://2D
	Sprite* ui = nullptr;

public:
	XMFLOAT3 playerPosition = { 1,0,0 };
	float cameraAngleX = 0;
	float cameraAngleY = 0;
	float difference;
	float attenRate = 0;
	//カメラと敵の逆行列の角度
	float radian = 0;
	//ラジアン変換
	float degree = 0;
	//敵の逆座標
	XMFLOAT3 inverseEnemyPos = { 0,0,0 };

	//当たり判定
	Sphere playerSp;
	Sphere enemySp;
	Sphere cameraSp;
	Sphere arcardSp[4];
	static const int maxWall_X = 30;
	static const int maxWall_Y = 6;
	Sphere frontWallSp[maxWall_Y][maxWall_X];
	Sphere backWallSp[maxWall_Y][maxWall_X];
	Sphere rightWallSp[maxWall_Y][maxWall_X];
	Sphere leftWallSp[maxWall_Y][maxWall_X];

	//ライト
	float ambientColor0[3] = { 1,1,1 };
	// 光線方向初期値
	float lightDir0[3] = { 0,0,-1 };
	float lightColor0[3] = { 5.8f,5.8f,5.8f };

	//スポットライト
	float spotLightDir[3] = { 0,-0.3f,0 };
	float spotLightColor[3] = { 1,1,0.8f };
	float spotLightAtten[3] = { 0.01f,0.01f,0.01f };
	float spotLightFactorAngle[2] = { 25.0f,30.0f };
	float p_spotLightPosY = 2.5f;
	float e_spotLightPosY = 4.5f;

	XMVECTOR spotLightDir_wall = { 15.7f,-3,-17.0f,0 };
	XMFLOAT3 spotLightPos_wall = { 0,0,0 };
	float spotLightAtten_wall[3] = { 0.02f,0.02f,0.02f };
	float spotLightFactorAngle_wall[2] = { 20,27.0f };
	float lightAngle = 0;
	float spotLightX = 0.1f;
	float easeTime = 0;
	float bfoTime = 0;
	bool isEase = false;

	//カメラ
	uint64_t shakex = 0;
	int shakey = 0;
	int shakeflag = 0;
	int count = 0;
	const int maxCount = 2;
	XMFLOAT3 cameraDis = { 0,0,0 };
	
	//ポストエフェクト
	PostEffect* postEffect = nullptr;
	int postEffectFlag = 0;

	//前ボーンワールド行列
	XMMATRIX oldMatWorld;
};
