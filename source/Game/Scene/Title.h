#pragma once

#include <DirectXMath.h>

#include<d3dx12.h>
#include<xaudio2.h>
#include<fstream>
#include"Input.h"
#include"WinApp.h"
#include"DirectXCommon.h"
#include"Object3d.h"
#include"Object3dModel.h"
#include"Sprite.h"
#include"DebugText.h" 
#include"Audio.h"
#include"fbxsdk.h"
#include"FbxLoader.h"
#include"FbxObject3d.h"
#include"PostEffect.h"
#include"Light.h"
#include"Effects.h"
#include"easing.h"
#include"debugObj.h"
#include "LightGroup.h"
#include"PostEffect.h"
#include"Calculation.h"
#include"BaseScene.h"


using namespace Microsoft::WRL;
using namespace DirectX;

class Title
{
private:
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;
public:
	Title();
	~Title();

	void Initialize(DirectXCommon* dxc, Audio* sound, Camera* camera, LightGroup* lightGroup, FbxObject3d* fbx);
	void Resource2dCreate();
	void GameInitialize();
	void Set(Camera* camera, FbxObject3d* fbx, XMFLOAT3 position, XMVECTOR distance, XMFLOAT3 target, XMFLOAT3 angle, std::string name);
	void Update();
	void Object3dDraw();
	void Resource2dDraw();

private:
	Easing* easing = nullptr;

private://FBXモデル

private://3Dモデル
	Object3dModel* boxModel = nullptr;
	Object3d* objBox = nullptr;

private://2D
	Sprite* title = nullptr;
	Sprite* clear = nullptr;
	Sprite* gameover = nullptr;
	Sprite* playerHP = nullptr;
	Sprite* blackBar = nullptr;
	Sprite* opFrame = nullptr;

private:
	Calculation* calculation;
	DirectXCommon* dxCommon;
	Audio* sound;
	Camera* camera;
	LightGroup* lightGroup;
	FbxObject3d* fbx;
public:
	//最大op
	static const int maxOp = 7;
	Sprite* op[maxOp];
	Sprite* logo;
	Sprite* boneLogo;
	Sprite* space;

	//op変数
	///神
	//切り替え<frm>
	static const int opChengeTime_ = 60 * 3;
	//切り替えイマー
	int32_t chengeTimer_1 = opChengeTime_;

	//肉ない
	static const int32_t opChengeTime_2 = 60 * 6;
	//切り替えイマー
	int32_t chengeTimer_2 = opChengeTime_2;

	//転ぶ
	FbxTime AnimationTime;
	//切り替えイマー
	int32_t chengeTimer_4 = opChengeTime_;

	//操作
	float ruleFrame = 0;
	//切り替えイマー
	int32_t chengeTimer_5 = opChengeTime_;

	//最高級
	float firstFrame = 0;
	//切り替えイマー
	int32_t chengeTimer_6 = opChengeTime_;

	//ともに
	//切り替えイマー
	int32_t chengeTimer_7 = opChengeTime_;

	//ロゴ
	float logoFrame = 0;
	static const int32_t opChengeTime_8 = 60 * 10;
	//切り替えイマー
	int32_t chengeTimer_8 = opChengeTime_8;

	//イージング
	const float frame = 0.01f;

	enum Op
	{
		god,	//神よ
		why,	//なぜ肉体
		trip,	//転ぶ
		revelation,	//お告げ
		rule,		//操作
		first_class,	//最高級の肉
		both,		//ともに
		logoTitle		//タイトル
	};

	int opNo = god;
};
