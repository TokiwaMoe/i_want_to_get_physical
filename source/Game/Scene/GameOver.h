#pragma once
#include"Sprite.h"

using namespace Microsoft::WRL;
using namespace DirectX;

class GameOver
{
private:
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;

public:
	GameOver();
	~GameOver();

	void Initialize();
	void Draw();

private:
	Sprite* gameover = nullptr;
};