#include "GameScene.h"
#include"Input.h"

GameScene::GameScene(){}
GameScene::~GameScene(){}

void GameScene::Initialize(DirectXCommon* dxc, Audio* sound, Camera* camera, LightGroup* lightGroup, FbxObject3d* fbx)
{
	this->camera = camera;
	this->dxCommon = dxc;
	this->lightGroup = lightGroup;
	this->audio = sound;
	this->fbx = fbx;

	Sprite::LoadTexture(10, L"Resource/rule.png");

	ui = Sprite::Create(10, { 0.0f,0.0f });
#pragma endregion

#pragma region パーティクル
	particleMan = ParticleManager::Create(dxc->GetDev(), WinApp::window_width, WinApp::window_height);
#pragma endregion

#pragma region サウンド
	/*audio = new Audio;
	audio->Initialize();*/
#pragma endregion

	for (int i = 0; i < MAX_SMOKE; i++)
	{
		effects[i] = new Effects();
		effects[i]->Initialize(dxc->GetDev(), dxc->GetCmdQueue(), this->camera);
		effects[i]->Load(L"effectsTest/10/smoke.efk");
	}

	player = new Player();
	player->Initialize(dxc, this->camera);
	enemy = new Enemy();
	enemy->Initialize(dxc, this->camera);
	sword = new Sword();
	sword->Initialize(enemy, dxc, this->camera);

	easing = new Easing();
	easing->Initialize();

	debugObj = new DebugObj();
	debugObj->Initialize();

	//ポストエフェクトの初期化
	postEffect = new PostEffect();
	postEffect->Initialize(L"Resource/shaders/BloomVS.hlsl", L"Resource/shaders/bloomPS.hlsl");

	resource = new Resource();
	resource->Initialize(dxc);
}

void GameScene::GameInitialize()
{
	player->Init();
	enemy->Init();
}

void GameScene::Update()
{
	//マウス座標
	POINT mousePos;
	GetCursorPos(&mousePos);

	LightUpdate();
	ObstacleCollision();

	//カメラ移動
	float deltaTime = 1.0f / 60.0f;
	XMFLOAT3 pos;
	attenRate = 5.0f;
	attenRate *= deltaTime;

	///Todo0 スピードは前方と右方の2つ必要
	const float speedForward = 3.0f;
	const float speedRight = 1.0f;

	///Todo1 プレイヤーから敵へのベクトルを求める
	XMVECTOR forwardVec = calculation->ForwardVector(speedForward, enemy->GetPosition(), player->GetPosition());
	XMVECTOR rightVec = calculation->ForwardVector(speedRight, enemy->GetPosition(), player->GetPosition());

	XMFLOAT3 enemyPos;
	if (enemy->GetState() == Enemy::STATE::ASSALT)
	{
		//プレイヤーと敵の位置を計算
		XMFLOAT3 oldPos = { player->GetPosition().x, 2.0f, player->GetPosition().z };
		difference = calculation->Difference(enemy->GetPosition(), player->GetPosition());

		//プレイヤーの後ろを通るようにカメラを動かす
		if (difference >= 0)
		{
			pos = calculation->AddVec(oldPos, rightVec);
			enemyPos = calculation->AddVec(enemy->GetPosition(), forwardVec);
		}
		else
		{
			pos = calculation->AddVec(oldPos, -rightVec);
			enemyPos = calculation->AddVec(enemy->GetPosition(), -forwardVec);
		}

		camera->Attenuation(enemyPos, pos, { 0,1.0f,-7.0f }, attenRate, { cameraAngleX, cameraAngleY, 0 }, easing);
	}
	else if (enemy->GetState() == Enemy::STATE::ATTACK)
	{
		//プレイヤーと敵の位置を計算
		XMFLOAT3 oldPos = { player->GetPosition().x, 2.0f, player->GetPosition().z };
		difference = calculation->Difference(enemy->GetPosition(), player->GetPosition());

		//プレイヤーの後ろを通るようにカメラを動かす
		if (difference >= 0)
		{
			enemyPos = calculation->AddVec(enemy->GetPosition(), forwardVec);
		}
		else
		{
			enemyPos = calculation->AddVec(enemy->GetPosition(), -forwardVec);
		}
		cameraDis = { 0,1.0f,-6.0f };
		DamageCamera();
		camera->Attenuation(enemyPos, oldPos, cameraDis, attenRate, { cameraAngleX, cameraAngleY, 0 }, easing);
	}
	else
	{
		pos = { player->GetPosition().x, 2.0f, player->GetPosition().z };
		enemyPos = calculation->AddVec(enemy->GetPosition(), forwardVec);
		cameraDis = { 0,1.0f,-5.0f };
		DamageCamera();
		camera->Attenuation(enemyPos, pos, cameraDis, attenRate, { cameraAngleX, cameraAngleY, 0 }, easing);
	}

	//エフェクト再生
	for (int i = 0; i < MAX_SMOKE; i++)
	{
		effects[i]->Play();
		effects[0]->SetPosition({ -16.5f,10,-0.3f });
		effects[1]->SetPosition({ -16.5f,10,-31.2f });
		effects[2]->SetPosition({ 16.7f,10,-0.3f });
		effects[3]->SetPosition({ 16.7f,10,-31.2f });
		effects[i]->SetScale({ 1.5f,1.5f,1.5f });
		effects[i]->SetRotation({ 1,0,0 });
		effects[i]->SetAngle(90);
		effects[i]->SetSpeed(3);
		effects[i]->Update(dxCommon->GetCmdList(), camera);
	}

	//particleMan->Update();
	enemy->ResourceUpdate();
	player->ResourceUpdate();
	GameUpdate();
	resource->Update();

	lightGroup->Update();
	camera->Update();
}

void GameScene::DamageCamera()
{
	if (player->GetState() == Player::DAMAGE)
	{
		shakeflag = true;
		cameraDis.x += shakex;
		cameraDis.y += shakey;
	}

	//シェイク
	if (shakeflag && count < maxCount) {
		count++;
		shakex = (int)calculation->Get_rand_range(-3, 6);
		shakey = (int)calculation->Get_rand_range(-3, 6);

		if (count == maxCount) {
			shakeflag = false;
			count = 0;
			shakex = 0;
			shakey = 0;
		}
	}
}

void GameScene::SetLightPos_Dir(XMFLOAT3 pos, XMVECTOR dir)
{
	//ライトの座標
	spotLightPos_wall = pos;
	//向き
	spotLightDir_wall = dir;
}

void GameScene::LightUpdate()
{
	const int LIGHT_MAX = 3;
	lightGroup->SetCircleShadowActive(0, false);
	lightGroup->SetCircleShadowActive(1, false);
	for (int i = 0; i < LIGHT_MAX; i++)
	{
		lightGroup->SetDirLightActive(i, false);
		lightGroup->SetPointLightActive(i, false);
	}
	const int SPOTLIGHT_MAX = 7;
	for (int i = 0; i < SPOTLIGHT_MAX; i++)
	{
		lightGroup->SetSpotLightActive(i, true);
	}

	//スポットライト設定
	lightGroup->SetSpotLightDir(0, XMVECTOR({ spotLightDir[0], spotLightDir[1], spotLightDir[2], 0 }));
	lightGroup->SetSpotLightPos(0, { player->GetPosition().x, p_spotLightPosY, player->GetPosition().z });
	lightGroup->SetSpotLightColor(0, XMFLOAT3(spotLightColor));
	lightGroup->SetSpotLightAtten(0, XMFLOAT3(spotLightAtten));
	lightGroup->SetSpotLightFactorAngle(0, XMFLOAT2(spotLightFactorAngle));

	lightGroup->SetSpotLightDir(1, XMVECTOR({ spotLightDir[0], spotLightDir[1], spotLightDir[2], 0 }));
	lightGroup->SetSpotLightPos(1, { enemy->GetPosition().x, e_spotLightPosY, enemy->GetPosition().z });
	lightGroup->SetSpotLightColor(1, XMFLOAT3(spotLightColor));
	lightGroup->SetSpotLightAtten(1, XMFLOAT3(spotLightAtten));
	lightGroup->SetSpotLightFactorAngle(1, XMFLOAT2(spotLightFactorAngle));

	for (int i = 2; i < SPOTLIGHT_MAX; i++)
	{
		lightGroup->SetSpotLightDir(i, spotLightDir_wall);
		lightGroup->SetSpotLightPos(i, spotLightPos_wall);
		lightGroup->SetSpotLightColor(i, XMFLOAT3(spotLightColor));
		lightGroup->SetSpotLightAtten(i, XMFLOAT3({ spotLightAtten_wall[0], spotLightAtten_wall[1],spotLightAtten_wall[2] }));
		lightGroup->SetSpotLightFactorAngle(i, XMFLOAT2(spotLightFactorAngle));

		const int x = 0, y = 1, z = 2;
		if (i == 2)
		{
			SetLightPos_Dir({ 8.0f, 10.0f,-5.5f }, { 15.7f,-3.0f,-17.0f,0 });
		}
		else if (i == 3)
		{
			SetLightPos_Dir({ 8.0f, 10.0f,-7.5f }, { 15.7f,-3.0f,-20.0f,0 });
		}
		else if (i == 4)
		{
			XMFLOAT3 prePos = { -0.02f,-0.1f,-0.06f };
			XMFLOAT3 bfoPos = { -0.08f, -0.1f, -0.06f };
			XMFLOAT3 pos = { 0,0,0 };

			const float frame = 0.01f;
			if (isEase)
			{
				bfoTime += frame;
				pos = easing->ease(bfoPos, prePos, bfoTime);
				easeTime = 0;

				if (bfoTime >= easing->maxflame)
				{
					bfoTime = 0;
					isEase = false;

				}
			}
			else
			{
				easeTime += frame;
				pos = easing->ease(prePos, bfoPos, easeTime);
				bfoTime = 0;

				if (easeTime >= easing->maxflame)
				{
					easeTime = 0;
					isEase = true;

				}
			}
			SetLightPos_Dir({ -4.0f, 11.0f,-29.0f }, { pos.x,pos.y,pos.z });
		}
		else if (i == 5)
		{
			SetLightPos_Dir({ -14.0f, 11.0f,-28.5f }, { 0.05f,-0.1f,-0.06f,0 });
		}
		else if (i == 6)
		{
			XMVECTOR distance = { 0.05f,0,0 };
			XMFLOAT3 v = calculation->RotationYVelocity(distance, lightAngle);
			SetLightPos_Dir({ 0, 15.0f,-15.0f }, { v.x,-0.3f,v.z,0 });
			//減衰
			spotLightAtten_wall[x] = 0.035f;
			spotLightAtten_wall[y] = 0.035f;
			spotLightAtten_wall[z] = 0.035f;
		}
	}
}

void GameScene::Object3dDraw()
{
#pragma region 3D描画
	postEffect->Draw(dxCommon->GetCmdList());
	dxCommon->ClearDepthBuffer();
	sword->Draw(dxCommon);
	enemy->Draw(dxCommon);
	player->Draw(dxCommon);
}

void GameScene::Resource2dDraw()
{
	float strX = 30;

	ui->Draw();
	player->SpriteDraw();
	enemy->DrawSprite();

	debugText.DrawAll(dxCommon->GetCmdList());
}

void GameScene::GameUpdate()
{
	enemy->Update(player, dxCommon, camera);
	player->Update(dxCommon, camera, enemy->GetPosition());
	sword->Update(player, enemy, dxCommon, camera);
}

void GameScene::PostEffectDraw()
{
#pragma region ポストエフェクト
	postEffect->PreDrawScene(dxCommon->GetCmdList());
	Object3d::PreDraw(dxCommon->GetCmdList());
	resource->EffectBackDraw();
	for (int i = 0; i < MAX_SMOKE; i++)
	{
		effects[i]->Draw(dxCommon->GetCmdList());
	}
	resource->EffectPreDraw();
	enemy->PostEffectDraw();
	Object3d::PostDraw();
	postEffect->PostDrawScene(dxCommon->GetCmdList());

#pragma endregion
}

void GameScene::Delete()
{
	FbxLoader::GetInstance()->Finalize();
	//DirectX解放
	delete dxCommon;
}

void GameScene::ObstacleCollision()
{
	//当たり判定
	//プレイヤーの中心と半径
	playerSp.center = XMLoadFloat3(&player->GetPosition());
	playerSp.radius = 0.8f;
	//敵の中心と半径
	// ボーンの計算済みワールド行列（ボーンのローカル行列×プレイヤーのワールド行列）
	XMMATRIX boneWorldMatrix = enemy->fbxEnemy->GetMatNowPose(enemy->fbxEnemy->GetBoneName("mixamorig:HeadTop_End")) * enemy->fbxEnemy->GetMatWorld();

	XMFLOAT3 pos = {
		boneWorldMatrix.r[3].m128_f32[0],
		0,
		boneWorldMatrix.r[3].m128_f32[2]
	};
	enemySp.center = XMLoadFloat3(&pos);
	enemySp.radius = 0.5f;
	//カメラの中心と半径
	cameraSp.center = XMLoadFloat3(&camera->GetEye());
	cameraSp.radius = 1.0f;

	//壁の中心と半径
	for (int j = 0; j < maxWall_Y; j++)
	{
		for (int i = 0; i < maxWall_X; i++)
		{
			//開始時プレイヤーから見て前
			frontWallSp[j][i].center = { -16.5f + i * frontWallSp[j][i].radius, j * frontWallSp[j][i].radius,0,0 };
			frontWallSp[j][i].radius = 1.5f;
			//開始時プレイヤーから見て後ろ
			backWallSp[j][i].center = { -16.5f + i * backWallSp[j][i].radius,j * backWallSp[j][i].radius, -31.5f,0 };
			backWallSp[j][i].radius = 1.5f;
			//開始時プレイヤーから見て右
			rightWallSp[j][i].center = { 15.5f - rightWallSp[j][i].radius,j * rightWallSp[j][i].radius, -31.5f + (i + 2) * rightWallSp[j][i].radius,0 };
			rightWallSp[j][i].radius = 1.5f;
			//開始時プレイヤーから見て左
			leftWallSp[j][i].center = { -16.5f - leftWallSp[j][i].radius, j * leftWallSp[j][i].radius, -31.5f + (i + 2) * leftWallSp[j][i].radius,0 };
			leftWallSp[j][i].radius = 1.5f;

			/// <summary>
			/// プレイヤーとの当たり判定
			/// </summary>
			XMVECTOR reject_pf = { 0,0,0,0 };
			PlayerCollision(playerSp, frontWallSp[j][i], reject_pf);
			PlayerCollision(playerSp, backWallSp[j][i], reject_pf);
			PlayerCollision(playerSp, rightWallSp[j][i], reject_pf);
			PlayerCollision(playerSp, leftWallSp[j][i], reject_pf);
			/// <summary>
			/// 敵との当たり判定
			/// </summary>
			XMVECTOR reject_ef = { 0,0,0,0 };
			EnemyCollision(enemySp, frontWallSp[j][i], reject_ef);
			EnemyCollision(enemySp, backWallSp[j][i], reject_ef);
			EnemyCollision(enemySp, rightWallSp[j][i], reject_ef);
			EnemyCollision(enemySp, leftWallSp[j][i], reject_ef);
			/// <summary>
			/// カメラとの当たり判定
			/// </summary>
			XMVECTOR reject_cf = { 0,0,0,0 };
			CameraCollision(cameraSp, frontWallSp[j][i], reject_cf);
			CameraCollision(cameraSp, backWallSp[j][i], reject_cf);
			CameraCollision(cameraSp, rightWallSp[j][i], reject_cf);
			CameraCollision(cameraSp, leftWallSp[j][i], reject_cf);

			debugObj->SetPosition({ leftWallSp[maxWall_Y - 1][maxWall_X - 1].center.m128_f32[0], leftWallSp[maxWall_Y - 1][maxWall_X - 1].center.m128_f32[1], leftWallSp[maxWall_Y - 1][maxWall_X - 10].center.m128_f32[2] });
		}
	}

	//敵とプレイヤーの当たり判定
	XMVECTOR reject_pe = { 0,0,0,0 };
	PlayerCollision(playerSp, enemySp, reject_pe);
}

void GameScene::PlayerCollision(Sphere p1, Sphere p2, XMVECTOR reject_)
{
	if (Collision::CheckSphere2(p1, p2, &reject_))
	{
		reject_.m128_f32[1] = 0;
		player->SetPosition(calculation->AddVec(player->GetPosition(), reject_));
	}
}

void GameScene::EnemyCollision(Sphere p1, Sphere p2, XMVECTOR reject_)
{
	if (Collision::CheckSphere2(p1, p2, &reject_))
	{
		reject_.m128_f32[1] = 0;
		enemy->SetPosition(calculation->AddVec(enemy->GetPosition(), reject_));
	}
}

void GameScene::CameraCollision(Sphere p1, Sphere p2, XMVECTOR reject_)
{
	if (Collision::CheckSphere2(p1, p2, &reject_))
	{
		reject_.m128_f32[1] = 0;
		camera->SetEye(calculation->AddVec(camera->GetEye(), reject_));
	}
}
