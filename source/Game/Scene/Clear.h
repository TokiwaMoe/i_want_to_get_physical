#pragma once
#include"FbxObject3d.h"
#include"Sprite.h"
#include"LightGroup.h"
#include"Camera.h"
#include"BaseScene.h"
#include"DirectXCommon.h"
#include"Audio.h"
#include"FbxObject3d.h"
#include "LightGroup.h"
#include"Calculation.h"

class Clear
{
private:
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;
public:
	Clear();
	~Clear();

	void Initialize(DirectXCommon* dxc, Audio* sound, Camera* camera, LightGroup* lightGroup, FbxObject3d* fbx);
	void Update();
	void Object3dDraw();
	void Resource2dDraw();

private:
	Calculation* calculation;
	DirectXCommon* dxCommon;
	Audio* sound;
	Camera* camera;
	LightGroup* lightGroup;
	FbxObject3d* fbx;

private:
	Sprite* clear = nullptr;

};
