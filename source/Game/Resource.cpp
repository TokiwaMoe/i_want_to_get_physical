#include "Resource.h"

Resource::Resource(){}
Resource::~Resource(){}

void Resource::Initialize(DirectXCommon* dxc)
{
	posterModel = Object3dModel::LoadFromOBJ("poster");
	posterModel_2 = Object3dModel::LoadFromOBJ("poster_2");
	boardModel = Object3dModel::LoadFromOBJ("board");
	boardModel_2 = Object3dModel::LoadFromOBJ("board_2");
	wallModel = Object3dModel::LoadFromOBJ("wall");
	signboardModel = Object3dModel::LoadFromOBJ("signboard");
	arcardModel = Object3dModel::LoadFromOBJ("arcard");
	arcardModel_2 = Object3dModel::LoadFromOBJ("arcard_2");
	boxModel = Object3dModel::LoadFromOBJ("box");
	sofaModel = Object3dModel::LoadFromOBJ("sofa");
	sofa_2Model = Object3dModel::LoadFromOBJ("sofa2");
	tableModel = Object3dModel::LoadFromOBJ("table");
	floorModel = Object3dModel::LoadFromOBJ("floor");
	redArcard_1Model = Object3dModel::LoadFromOBJ("redArcard_1");
	redArcard_2Model = Object3dModel::LoadFromOBJ("redArcard_2");
	redArcard_3Model = Object3dModel::LoadFromOBJ("redArcard_3");
	redArcard_gameModel = Object3dModel::LoadFromOBJ("redArcard_game");
	redArcard2_gameModel = Object3dModel::LoadFromOBJ("redArcard2_game");
	arcard_gameModel = Object3dModel::LoadFromOBJ("arcard_game");
	arcard2_gameModel = Object3dModel::LoadFromOBJ("arcard2_game");

	poster = Object3d::Create();
	poster->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	poster->SetObject3dModel(posterModel);
	poster_2 = Object3d::Create();
	poster_2->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	poster_2->SetObject3dModel(posterModel_2);
	objBoard = Object3d::Create();
	objBoard->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objBoard->SetObject3dModel(boardModel);
	objBoard_2 = Object3d::Create();
	objBoard_2->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objBoard_2->SetObject3dModel(boardModel_2);
	objWall = Object3d::Create();
	objWall->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objWall->SetObject3dModel(wallModel);
	objSignboard = Object3d::Create();
	objSignboard->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objSignboard->SetObject3dModel(signboardModel);
	objArcard = Object3d::Create();
	objArcard->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objArcard->SetObject3dModel(arcardModel);
	objArcard_2 = Object3d::Create();
	objArcard_2->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objArcard_2->SetObject3dModel(arcardModel_2);
	objBox = Object3d::Create();
	objBox->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objBox->SetObject3dModel(boxModel);
	objSofa = Object3d::Create();
	objSofa->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objSofa->SetObject3dModel(sofaModel);
	objSofa_2 = Object3d::Create();
	objSofa_2->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objSofa_2->SetObject3dModel(sofa_2Model);
	objTable = Object3d::Create();
	objTable->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objTable->SetObject3dModel(tableModel);
	objFloor = Object3d::Create();
	objFloor->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objFloor->SetObject3dModel(floorModel);

	objRedArcard_1 = Object3d::Create();
	objRedArcard_1->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objRedArcard_1->SetObject3dModel(redArcard_1Model);
	objRedArcard_2 = Object3d::Create();
	objRedArcard_2->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objRedArcard_2->SetObject3dModel(redArcard_2Model);
	objRedArcard_3 = Object3d::Create();
	objRedArcard_3->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objRedArcard_3->SetObject3dModel(redArcard_3Model);
	objRedArcard2_1 = Object3d::Create();
	objRedArcard2_1->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objRedArcard2_1->SetObject3dModel(redArcard_1Model);
	objRedArcard2_2 = Object3d::Create();
	objRedArcard2_2->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objRedArcard2_2->SetObject3dModel(redArcard_2Model);
	objRedArcard2_3 = Object3d::Create();
	objRedArcard2_3->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objRedArcard2_3->SetObject3dModel(redArcard_3Model);
	objRedArcard_game = Object3d::Create();
	objRedArcard_game->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objRedArcard_game->SetObject3dModel(redArcard_gameModel);
	objRedArcard2_game = Object3d::Create();
	objRedArcard2_game->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objRedArcard2_game->SetObject3dModel(redArcard2_gameModel);
	objArcard_game = Object3d::Create();
	objArcard_game->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objArcard_game->SetObject3dModel(arcard_gameModel);
	objArcard2_game = Object3d::Create();
	objArcard2_game->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPS_Light.hlsl");
	objArcard2_game->SetObject3dModel(arcard2_gameModel);

	poster->SetScale({ 1.5f,1.5f,1.5f });
	poster_2->SetScale({ 1.5f,1.5f,1.5f });
	objBoard->SetScale({ 1.5f,1.5f,1.5f });
	objBoard_2->SetScale({ 1.5f,1.5f,1.5f });
	objWall->SetScale({ 1.5f,1.5f,1.5f });
	objWall->SetPosition({ 0,-0.6f,0 });
	objSignboard->SetScale({ 1.5f,1.5f,1.5f });
	objArcard->SetScale({ 1.5f,1.5f,1.5f });
	objArcard_game->SetScale({ 1.5f,1.5f,1.5f });
	objArcard_game->SetPosition({ 0,0,0.1f });
	objArcard2_game->SetScale({ 1.5f,1.5f,1.5f });
	objArcard_2->SetScale({ 1.5f,1.5f,1.5f });
	objBox->SetRotation({ 0,240,0 });
	objSofa->SetScale({ 1.5f,1.5f,1.5f });
	objSofa_2->SetScale({ 1.5f,1.5f,1.5f });
	objTable->SetScale({ 1.5f,1.5f,1.5f });
	objFloor->SetScale({ 1.5f,1.5f,1.5f });
	objFloor->SetPosition({ 0,-0.5f,0 });

	//レッドアーケード1
	objRedArcard_1->SetScale({ 1.5f,1.5f,1.5f });
	objRedArcard_1->SetPosition({ 0,-0.5f,-2.3f });
	objRedArcard_2->SetScale({ 1.5f,1.5f,1.5f });
	objRedArcard_2->SetPosition({ 0,-0.5f,-2.3f });
	objRedArcard_3->SetScale({ 1.5f,1.5f,1.5f });
	objRedArcard_3->SetPosition({ 0,-0.5f,-2.3f });
	objRedArcard_game->SetScale({ 1.5f,1.5f,1.5f });
	objRedArcard_game->SetPosition({ 0,-0.5f,-2.3f });
	//レッドアーケード2
	objRedArcard2_1->SetScale({ 1.5f,1.5f,1.5f });
	objRedArcard2_1->SetPosition({ 0,-0.5f,1.0f });
	objRedArcard2_2->SetScale({ 1.5f,1.5f,1.5f });
	objRedArcard2_2->SetPosition({ 0,-0.5f,1.0f });
	objRedArcard2_3->SetScale({ 1.5f,1.5f,1.5f });
	objRedArcard2_3->SetPosition({ 0,-0.5f,1.0f });
	objRedArcard2_game->SetScale({ 1.5f,1.5f,1.5f });
	objRedArcard2_game->SetPosition({ 0,-0.5f,4.0f });
}

void Resource::Update()
{
	poster->Update();
	poster_2->Update();
	objBoard->Update();
	objBoard_2->Update();
	objSignboard->Update();
	objArcard->Update();
	objArcard_2->Update();
	objWall->Update();
	objSofa_2->Update();
	objSofa->Update();
	objTable->Update();
	objFloor->Update();
	objRedArcard_1->Update();
	objRedArcard_2->Update();
	objRedArcard_3->Update();
	objRedArcard2_1->Update();
	objRedArcard2_2->Update();
	objRedArcard2_3->Update();
	objRedArcard_game->Update();
	objRedArcard2_game->Update();
	objArcard_game->Update();
	objArcard2_game->Update();
}

void Resource::EffectPreDraw()
{
	objBoard->Draw();
	objSignboard->Draw();
	objArcard->Draw();
	objArcard_2->Draw();
	objRedArcard_game->Draw();
	objRedArcard2_game->Draw();
	objArcard_game->Draw();
	objArcard2_game->Draw();
	objRedArcard_1->Draw();
	objRedArcard_2->Draw();
	objRedArcard_3->Draw();
	objRedArcard2_1->Draw();
	objRedArcard2_2->Draw();
	objRedArcard2_3->Draw();
	objSofa->Draw();
	objSofa_2->Draw();
	objTable->Draw();
	objFloor->Draw();
	objBoard_2->Draw();
}

void Resource::EffectBackDraw()
{
	objWall->Draw();
	poster->Draw();
	poster_2->Draw();
}

void Resource::Draw()
{
}
