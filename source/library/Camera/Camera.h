﻿#pragma once

#include <DirectXMath.h>
#include"Easing.h"

/// <summary>
/// カメラ基本機能
/// </summary>
class Camera
{
protected: // エイリアス
	// DirectX::を省略
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMVECTOR = DirectX::XMVECTOR;
	using XMMATRIX = DirectX::XMMATRIX;

public: // メンバ関数
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name="window_width">画面幅</param>
	/// <param name="window_height">画面高さ</param>
	Camera(int window_width, int window_height);

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~Camera() = default;

	/// <summary>
	/// 毎フレーム更新
	/// </summary>
	virtual void Update();

	/// <summary>
	/// ビュー行列を更新
	/// </summary>
	void UpdateViewMatrix();

	/// <summary>
	/// 射影行列を更新
	/// </summary>
	void UpdateProjectionMatrix();

	/// <summary>
	/// ビュー行列の取得
	/// </summary>
	/// <returns>ビュー行列</returns>
	inline const XMMATRIX& GetViewMatrix() {
		return matView;
	}

	/// <summary>
	/// 射影行列の取得
	/// </summary>
	/// <returns>射影行列</returns>
	inline const XMMATRIX& GetProjectionMatrix() {
		return matProjection;
	}

	/// <summary>
	/// ビュー射影行列の取得
	/// </summary>
	/// <returns>ビュー射影行列</returns>
	inline const XMMATRIX& GetViewProjectionMatrix() {
		return matViewProjection;
	}

	/// <summary>
	/// ビルボード行列の取得
	/// </summary>
	/// <returns>ビルボード行列</returns>
	inline const XMMATRIX& GetBillboardMatrix() {
		return matBillboard;
	}

	/// <summary>
	/// 視点座標の取得
	/// </summary>
	/// <returns>座標</returns>
	inline const XMFLOAT3& GetEye() {
		return eye;
	}

	/// <summary>
	/// 視点座標の設定
	/// </summary>
	/// <param name="eye">座標</param>
	inline void SetEye(XMFLOAT3 eye) {
		this->eye = eye; viewDirty = true;
	}

	/// <summary>
	/// 注視点座標の取得
	/// </summary>
	/// <returns>座標</returns>
	inline const XMFLOAT3& GetTarget() {
		return target;
	}

	/// <summary>
	/// 注視点座標の設定
	/// </summary>
	/// <param name="target">座標</param>
	inline void SetTarget(XMFLOAT3 target) {
		this->target = target; viewDirty = true;
	}

	/// <summary>
	/// 上方向ベクトルの取得
	/// </summary>
	/// <returns>上方向ベクトル</returns>
	inline const XMFLOAT3& GetUp() {
		return up;
	}

	/// <summary>
	/// 上方向ベクトルの設定
	/// </summary>
	/// <param name="up">上方向ベクトル</param>
	inline void SetUp(XMFLOAT3 up) {
		this->up = up; viewDirty = true;
	}

	/// <summary>
	/// ベクトルによる視点移動
	/// </summary>
	/// <param name="move">移動量</param>
	void MoveEye(const XMFLOAT3& move);
	void MoveEyeVector(const XMVECTOR& move);

	/// <summary>
	/// ベクトルによる移動
	/// </summary>
	/// <param name="move">移動量</param>
	void Move(const XMFLOAT3& move);
	void MoveVector(const XMVECTOR& move);

	/// <summary>
	/// 注視点を中心に回転
	/// </summary>
	void TargetRot(XMVECTOR distance, XMFLOAT3 target, XMFLOAT3 angle);

	/// <summary>
	/// 減衰
	/// </summary>
	void Attenuation(XMFLOAT3 followingPos, XMFLOAT3 targetPos, XMFLOAT3 distance, float attenRate, XMFLOAT3 eulerAngles, Easing *easing);

protected: // メンバ変数
	// ビュー行列
	XMMATRIX matView = DirectX::XMMatrixIdentity();
	// ビルボード行列
	XMMATRIX matBillboard = DirectX::XMMatrixIdentity();
	// Y軸回りビルボード行列
	XMMATRIX matBillboardY = DirectX::XMMatrixIdentity();
	// 射影行列
	XMMATRIX matProjection = DirectX::XMMatrixIdentity();
	// ビュー射影行列
	XMMATRIX matViewProjection = DirectX::XMMatrixIdentity();
	
	// ビュー行列ダーティフラグ
	bool viewDirty = false;
	// 射影行列ダーティフラグ
	bool projectionDirty = false;
	// 視点座標
	XMFLOAT3 eye = {0, 0, -20};
	// 注視点座標
	XMFLOAT3 target = {0, 0, 0};
	// 上方向ベクトル
	XMFLOAT3 up = {0, 1, 0};
	// アスペクト比
	float aspectRatio = 1.0f;
	XMFLOAT3 cameraTarget;
	XMFLOAT3 cameraEye;

public:
	//回転
	XMMATRIX matCameraRot;
	//減衰比率
	float AttenRate;
	//時間
	float time = 0;
	//到達座標
	XMFLOAT3 pos = { 0,0,0 };
	//1秒間180度
	XMFLOAT2 rotationSpeed = { 180,180 };
	//1/60.0f
	float fixedDeltaTime = 1 / 60.0f;
	//追跡するものとターゲットするものの距離
	float follow_targetPosX;
	float follow_targetPosZ;
	//長さ
	float len = 0;
	//ベクトル
	float dx;
	float dz;
	//距離
	float posX;
	float posZ;
	//外積計算用
	XMVECTOR y;
	XMVECTOR vY;
	XMVECTOR z;
	XMVECTOR vZ;
	XMVECTOR evY;
	XMVECTOR evZ;
	XMVECTOR evX;
};

