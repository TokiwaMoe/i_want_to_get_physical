#include "debugObj.h"

void DebugObj::Initialize()
{
	sphereModel = Object3dModel::LoadFromOBJ("sphere");
	objSphere = Object3d::Create();
	objSphere->InitializeGraphicsPipeline(L"Resource/shaders/OBJVertexShader.hlsl", L"Resource/shaders/OBJPixelShader.hlsl");
	objSphere->SetObject3dModel(sphereModel);
}

void DebugObj::Update()
{
	objSphere->SetPosition(position);
	objSphere->SetRotation(rotation);
	objSphere->SetScale(scale);
	objSphere->Update();
}

void DebugObj::Draw()
{
	objSphere->Draw();
}
