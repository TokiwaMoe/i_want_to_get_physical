#pragma once
#include"FbxObject3d.h"
#include"Sprite.h"
#include"LightGroup.h"
#include"Camera.h"
#include"DirectXCommon.h"
#include"Audio.h"

class BaseScene
{
public:
	virtual void Initialize(DirectXCommon* dxc, Audio* sound, Camera* camera, LightGroup* lightGroup, FbxObject3d* fbx) = 0;
	virtual void Update(FbxObject3d* fbx, Camera* camera, LightGroup* lightGroup)=0;
	virtual void Object3dDraw(DirectXCommon* dxCommon, FbxObject3d* fbx)=0;
	virtual void Resource2dDraw()=0;
};
