#pragma once
#include"FbxModel.h"
#include"FbxLoader.h"
#include"Camera.h"
#include"easing.h"
#include "LightGroup.h"

#include<Windows.h>
#include<wrl.h>
#include<d3d12.h>
#include<d3dx12.h>
#include<DirectXMath.h>
#include<string>

class FbxObject3d
{
protected://エイリアス
	//Microsoft::WRL::を省略
	template<class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	//DirectX::を省略
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;

public://定数
	//ボーンの最大数
	static const int MAX_BONES = 128;

public://サブクラス
	//定数バッファ用データ構造体(座標変換行列用)
	struct ConstBufferDataTransform
	{
		XMMATRIX viewproj;		//ビュープロジェクション行列
		XMMATRIX world;			//ワールド行列
		XMFLOAT3 cameraPos;		//カメラ座標(ワールド座標)
	};

	//定数バッファ用データ構造体(スキニング)
	struct ConstBufferDataSkin
	{
		XMMATRIX bones[MAX_BONES];
	};

	struct AnimationData
	{
		FbxTakeInfo* takeinfo;
		FbxAnimStack* animeStack;
		//アニメーション開始時間
		FbxTime startTime;
		//アニメーション終了時間
		FbxTime endTime;
		//現在時間(アニメーション)
		FbxTime currentTime;
	};

public://静的メンバ関数
	//setter
	static void SetDevice(ID3D12Device* device) { FbxObject3d::device = device; }
	static void SetCamera(Camera* camera) { FbxObject3d::camera = camera; }

public://メンバ変数
	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize();

	/// <summary>
	/// グラフィックパイプラインの生成
	/// </summary>
	static void CreateGraphicsPipline();

	/// <summary>
	/// 毎フレーム処理
	/// </summary>
	void Update();

	/// <summary>
	/// モデルのセット
	/// </summary>
	/// <param name="model">モデル</param>
	void SetModel(FbxModel* model) { this->model = model; }

	/// <summary>
	/// 描画
	/// </summary>
	void Draw(ID3D12GraphicsCommandList* cmdList);

	/// <summary>
	/// 補間
	/// </summary>
	void Interpolation(FbxModel *startModel, FbxModel *endModel, int startNum, int endNum);

	/// <summary>
	/// アニメーション再生
	/// </summary>
	void PlayAnimation(int num);

	void LoadAnumation();
	//setter
	//座標
	void SetPosition(XMFLOAT3 position) { this->position = position; }
	//回転
	void SetRotation(XMFLOAT3 rotation) { this->rotation = rotation; }
	//大きさ
	void SetScale(XMFLOAT3 scale) { this->scale = scale; }
	//時間セット
	void SetNowTime(FbxTime nowTime) { this->animeDatas_[armatureNo].currentTime = nowTime; }
	//回転行列
	void SetMatRot(XMMATRIX matRot) { this->acMatRot = matRot; }
	//
	void SetIsRot(bool flag) { this->isRot = flag; }
	//アーマチュア番号
	void SetArmatureNo(int num) { this->armatureNo = num; }
	//補間フラグ
	void SetIsLerp(bool islerp) { this->isLerp = islerp; }
	//再生フラグ
	void SetIsPlay(bool isPlay) { this->isPlay = isPlay; }
	/// <summary>
	/// ライトグループのセット
	/// </summary>
	/// <param name="lightGroup">ライトグループ</param>
	static void SetLightGroup(LightGroup* lightGroup) {
		FbxObject3d::lightGroup = lightGroup;
	}
	//getter
	//回転行列
	XMMATRIX GetMatRot() { return matRot; }
	//回転
	XMFLOAT3 GetRotation() { return rotation; }
	//座標
	XMFLOAT3 GetPosition() { return position; }
	//開始時間
	FbxTime GetStartTime() { return animeDatas_[armatureNo].startTime; }
	//終わり時間
	FbxTime GetEndTime() { return animeDatas_[armatureNo].endTime; }
	//1フレームの時間
	FbxTime GetFrame() { return frameTime; }
	//現在の時間
	FbxTime GetNowTime() { return animeDatas_[armatureNo].currentTime; }
	//現在のポーズ
	XMMATRIX GetMatNowPose(int num) { return matCurrentPose[num]; }
	//ボーンの名前
	int GetBoneName(std::string name);
	//ワールド座標
	XMMATRIX GetMatWorld() { return matWorld; }
	//アーマチュアの番号検索
	int GetArmature(std::string name);
	//アーマチュアの番号取得
	int GetArmatureNo() { return armatureNo; }
	//補間フラグ
	bool GetIsLerp() { return isLerp; }

protected://メンバ変数
	//定数バッファ
	ComPtr<ID3D12Resource>  constBuffTransform;
	//ルートシグネチャ
	static ComPtr<ID3D12RootSignature> rootsignature;
	//パイプラインステートオブジェクト
	static ComPtr<ID3D12PipelineState> pipelinestate;
	//定数バッファ(スキン)
	ComPtr<ID3D12Resource> constBuffSkin;

private://静的メンバ変数
	//デバイス
	static ID3D12Device* device;
	//カメラ
	static Camera* camera;
	// ライト
	static LightGroup* lightGroup;

protected://メンバ変数
	//ローカルスケール
	XMFLOAT3 scale = { 1,1,1 };
	//X,Y,Z軸回りのローカル回転角
	XMFLOAT3 rotation = { 0,0,0 };
	//ローカル座標
	XMFLOAT3 position = { 0,0,0 };
	//ローカルワールド変換行列
	XMMATRIX matWorld;
	//モデル
	FbxModel* model = nullptr;
	//1フレームの時間
	FbxTime frameTime;
	//アニメーション再生
	bool isPlay = false;

	Node* node = nullptr;
	//今の姿勢行列
	XMMATRIX matCurrentPose[MAX_BONES];
	XMMATRIX matNowPose;
	ConstBufferDataSkin* constMapSkin = nullptr;

	std::string name;
	//行列
	XMMATRIX matScale, matRot, matTrans;
	//取得用
	XMMATRIX acMatRot;
	//
	bool isRot = false;
	//複数アニメーション用
	std::vector<AnimationData> animeDatas_ = {};
	//シーン
	FbxScene* scene;
	//アーマチュア番号
	int armatureNo = 0;
	//補間時間
	const float maxLerpTime = 1.0f;
	XMMATRIX lerpPose[MAX_BONES] = {};
	bool isLerp = false;
};
